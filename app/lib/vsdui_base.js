/*
 * (c) Volker Schulte volkerschulte@gmail.com
 * Änderungswünsche willkommen.
 *
 */

/**
 * hallo vsbase
 * @module vsdui_base
 */

import vsdui_window from './vsdui_window.js'

/** class vsdui_base */
export default class vsdui_base {
    /**
     *
     * @param {string} id
     * @param {number} x
     * @param {number} y
     * @param {number} w
     * @param {number} h
     * @param {string} elem
     * @returns {vsdui_base}
     */
    constructor(id, x, y, w, h, elem) {
        this.that = document.createElement(elem || 'div')
        this.that._id = id
        this.that._x = x
        this.that._y = y
        this.that._w = w
        this.that._h = h
        this.that._position = 'absolute'
        this.that._style = {
            'box-sizing': 'border-box',
            'font-family': 'Monaco, monospace',
            'background-color': 'white',
            'color': '#900000',
            'font-size': '16px',
            'border': '0px solid',
            'padding': '0px',
            'margin': '0px'
        }
        this.that._content = ''
        this._render()
    }
    /**
     * @returns {vsdui_base}
     */
    _render() {
        try {
            this.that.id = this.that._id
            this.setposition(this.getposition())
            this.setstyle(this.that._style)
            this.addcontent(this.that._content)
            return this
        } catch (e) {
            console.log(e)
        }
    }
    /**
     *
     *  objekt mit css-styles übergeben
     * @param {Object} obj
     * @returns {vsdui_base}
     */
    setstyle(obj) {
        for (let v in obj) {
            try {
                this.that._style[v] = obj[v]
            } catch (exception) {
                console.log(exception)
            }
        }
        ;
        for (let v in this.that._style) {
            try {
                this.that.style[v] = this.that._style[v]
            } catch (exception) {
                console.log(exception)
            }
        }
        ;
        return this
    }
    /**
     *
     *
     *  position setzen
     * @param {object} o
     * @returns {vsdui_base}
     */
    setposition(o) {
        const px = 'px'
        this.that._x = o.x || this.that._x
        this.that._y = o.y || this.that._y
        this.that._w = o.w || this.that._w
        this.that._h = o.h || this.that._h
        this.that._position = 'absolute'
        this.that.style.position = this.that._position
        this.that.style.top = this.that._y + px
        this.that.style.left = this.that._x + px
        this.that.style.width = this.that._w + px
        this.that.style.height = this.that._h + px
        return this
    }
    /**
     *
     *  holt die position der komponente
     * @returns {vsdui_base.getposition.o}
     */

    getposition() {
        let o =  {
            x: this.that._x,
            y: this.that._y,
            w: this.that._w,
            h: this.that._h
        }
        return o

    }
    /**
     *
     *  der name sagt alles
     * @returns {vsdui_base}
     */
    center_horizontal(p) {
        let b = vsdui_window.getwidth() / 2
        let x = vsdui_window.getvonlinks() + (b - (this.getposition().w / 2))
        //parentelement ist belegt
        if (p && p != null ) {
            // this ist childnode von p

            if (p.getcomponent().contains(this.getcomponent())) {
                let w = p.getposition().w / 2
                x = w - this.getposition().w / 2       
            } else {
                //this ist nicht childnode von p, soll aber trotzdem zentriert werden
                let w = p.getposition().w / 2
                x = p.getposition().x + w - this.getposition().w / 2       
            }
 
        }
        this.setposition({
            x: x
        })
        return this
    }
    /**
     *
     *  der name sagt alles
     * @returns {vsdui_base}
     */
    center_vertical(p) {
        let h = vsdui_window.getheight() / 2
        let y = vsdui_window.getvonoben() + (h - (this.getposition().h / 2))
        //parentelement ist belegt
        if (p && p != null) {
            // this ist childnode von p
            if (p.getcomponent().contains(this.getcomponent())) {
                h = p.getposition().h / 2
                y = h - this.getposition().h / 2
            }
            else{
                //this ist nicht childnode von p, soll aber trotzdem zentriert werden
                h = p.getposition().h / 2
                y = p.getposition().y + h - this.getposition().h / 2

            }
          
        }
        this.setposition({
            y: y
        })
        return this
    }
    /**
     *
     *  der name sagt alles
     * @returns {vsdui_base}
     */
    center_both(p) {
        this.center_horizontal(p)
        this.center_vertical(p)
        return this
    }
    /**
     *
     *  childnodes bzw. innerhtml hinzufügen
     * @param {Element|String} c
     * @returns {vsdui_base}
     */
    addcontent(c) {
        this.that._content = c
        if (this.that._content instanceof Element) {
            this.that.appendChild(this.that._content)
        } else {
            this.that.innerHTML = this.that._content
        }
        this.that._content = this.that.innerHTML
        return this
    }
    /**
     *
     *  das eigentliche domobjekt zurückgeben
     * @returns {Element}
     */
    getcomponent() {
        return this.that
    }
    /**
     *
     *  die komponente aus dem dom entfernen
     * @returns {undefined}
     */
    removefromdom() {

        var t = this.that
        t.parentNode.removeChild(t)
    }
    /**
     *
     * die komponente in das dom einfügen
     * @param {element} elem
     * @returns {vsdui_base}
     */
    appendtodom(elem) {
        elem.appendChild(this.that)
        return this
    }
}
