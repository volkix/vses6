/*
 * (c) Volker Schulte volkerschulte@gmail.com
 * Änderungswünsche willkommen.
 *
 */

/**
*
* @module vsdui_panel
* @extends vsdui_base
*
*
*/

import vsdui_base from './vsdui_base.js'
import vsdui_button from './vsdui_button.js'
import vsdui_label from './vsdui_label.js'
import vsevent from './vsevent.js'

export default class vsdui_panel extends vsdui_base {
  /**
   * vsdui_panel
   *
   * @param {string} id
   * @param {string} ueberschrift
   * @param {number} x
   * @param {number} y
   * @param {number} w
   * @param {number} h
   */
  constructor(id, ueberschrift, x, y, w, h) {
    super(id, x, y, w, h, 'div')
    const o = {
      'font-weight': 'bold',
      'background-color': 'white',
      'color': '#900000',
      'border': '1px solid silver',
      'overflow': 'auto'

    }
    this.setstyle(o)
    this._ueber = new vsdui_label(id + '-bue', ueberschrift, 0, 0, w-2, 26)
    this._ueber.setstyle({
      'background-color': '#0e7a0c',
      'color': 'white',
      'text-align': 'center'
    })
    this._cont = new vsdui_base(id + '-con', 0, 28, w - 2, h - 30)
    this._cont.setstyle({
      'background-color': 'white',
      'color': '#900000',
      'border': '0px solid',
      'overflow': 'auto'
    })
    this.addcontent(this._ueber.getcomponent())
    this.addcontent(this._cont.getcomponent())
  }
  /**
   * gibt die content-komponente zurück
   * @returns {vsdui_panel._cont}
   */
  getcontent() {
    return this._cont
  }
}
