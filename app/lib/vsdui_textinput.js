/*
 * (c) Volker Schulte volkerschulte@gmail.com
 * Änderungswünsche willkommen.
 *
 */


/**
 *
 * @module vsdui_textinput
 * @extends vsdui_base
 *
 */

import vsevent from './vsevent.js'
import vsdui_base from './vsdui_base.js'

export default class vsdui_textinput extends vsdui_base {
    /**
     * vsdui_textinput
     *
     * @param {string} id
     * @param {number} x
     * @param {number} y
     * @param {number} w
     * @param {number} h
     * @returns {vsdui_textinput}
     */
    constructor(id, x, y, w, h) {
        super(id, x, y, w, h, 'input')
        const o = {
            'padding': '3px',
            'background-color': 'white',
            'color': '#900000',
            'text-align': 'left',
            'border': '1px solid silver'
        }
        this.setstyle(o)
        let c = this.that
        let ct = this
        ct.formatter = function (v) {
            return v
        }
        c.setAttribute('placeholder', '?')
        c.addEventListener('focus', e => {
            ct.aktvalue = c.value

        }, false)
        c.addEventListener('blur', e => {
            ct.altvalue = ct.aktvalue
            c.value = ct.formatter(c.value)
            ct.aktvalue = c.value

            if (ct.aktvalue !== ct.altvalue) {
                console.log(ct.aktvalue, ct.altvalue)
                vsevent.send('changevalue_' + id, {
                    'aktwert': ct.aktvalue,
                    'altwert': ct.altvalue,
                    'id': id
                })
            }
        }, false)
    }
    /**
     *
     * setzen des feldinhaltes
     * @param {string} v
     * @returns {undefined}
     */
    setvalue(v) {

        let c = this.that
        let ct = this
        c.value = v

        ct.altvalue = ct.aktvalue
        c.value = ct.formatter(c.value)
        ct.aktvalue = c.value
        if (ct.aktvalue !== ct.altvalue) {
            vsevent.send('changevalue_' + c._id, {
                'aktwert': ct.aktvalue,
                'altwert': ct.altvalue,
                'id': c._id
            })
        }
    }
    /**
     *
     * lesen des feldinhaltes
     * @returns {String}
     */
    getvalue() {
        return this.that.value
    }
    /**
     * funktion mit einem parameter übergeben, die für die
     * Formatierung des feldinhaltes sorgt
     * @param {function} f
     * @returns {undefined}
     */
    setformatter(f) {
        this.formatter = f
    }
}
