/*
 * (c) Volker Schulte volkerschulte@gmail.com
 * Änderungswünsche willkommen.
 *
 */

/**
*
* @module vsdui_textarea
* @extends vsdui_base
*
*
*/


import vsevent from './vsevent.js'
import vsdui_base from './vsdui_base.js'

export default class vsdui_textarea extends vsdui_base {
  /**
   * vsdui_textarea
   *
   * @param {string} id
   * @param {number} x
   * @param {number} y
   * @param {number} w
   * @param {number} h
   */
  constructor(id, x, y, w, h) {
    super(id, x, y, w, h, 'textarea')
    const o = {
      'padding': '3px',
      'background-color': 'white',
      'color': '#900000',
      'text-align': 'left',
      'border': '1px solid silver',
      'resize': 'none'
    }
    this.setstyle(o)
    let c = this.that
    let ct = this
    c.setAttribute('placeholder', '?')
    c.addEventListener('focus', e => {
      ct.aktvalue = c.value
    }, false)
    c.addEventListener('blur', e => {
      ct.altvalue = ct.aktvalue
      ct.aktvalue = c.value
      if (ct.aktvalue !== ct.altvalue) {
        vsevent.send('changevalue_' + id, {
          'aktwert': ct.aktvalue,
          'altwert': ct.altvalue,
          'id': id
        })
      }
    }, false)
  }
  /**
   *
   * setzes des feldinhaltes
   * @param {string} v
   * @returns {undefined}
   */
  setvalue(v) {
    this.that.value = v
  }
  /**
   *
   * holen des feldinhaltes
   * @returns {String}
   */
  getvalue() {
    return this.that.value
  }
}
