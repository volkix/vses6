/*
 * (c) Volker Schulte volkerschulte@gmail.com
 * Änderungswünsche willkommen.
 *
 */


/**
 *
 * @module vsdui_datepicker
 * @extends vsdui_panel
 *
 *
 */

import vsdui_panel from './vsdui_panel.js'
import vsdui_button from './vsdui_button.js'
import vsdui_textoutput from './vsdui_textoutput.js'
import vsdui_label from './vsdui_label.js'

import vsdom from './vsdom.js'
import vsdate from './vsdate.js'
import vsevent from './vsevent.js'


export default class vsdui_datewidget extends vsdui_panel {
    /**
     * vsdui_datepicker
     * @description datumsauswahlbox
     * <pre>
     * sendet: 'datepicker' mit ID und VALUE
     * </pre>
     *
     * @param {string} id
     * @param {string} ueberschrift
     * @param {number} x
     * @param {number} y
     * @param {number} w
     * @param {number} h
     */
    constructor(id, ueberschrift, x, y, w, h) {
        super(id, ueberschrift, x, y, w, h)
        this._cbreite = this.getcontent().getposition().w - 10
        this._choehe = this.getcontent().getposition().h - 10
        this._bdrittel = (this._cbreite - 10) / 3
        this._hfuenftel = (this._choehe - 9) / 3
        this._anfang = 5
        this._comps = {
            bttp: new vsdui_button(id + '-bttm', '-', this._anfang, this._anfang + 1 * this._hfuenftel + 3, this._bdrittel / 2, this._hfuenftel),
            bmmp: new vsdui_button(id + '-bmmm', '-', this._anfang + this._bdrittel + 5, this._anfang + 1 * this._hfuenftel + 3, this._bdrittel / 2, this._hfuenftel),
            bjjp: new vsdui_button(id + '-bjjm', '-', this._anfang + this._bdrittel + this._bdrittel + 10, this._anfang + 1 * this._hfuenftel + 3, this._bdrittel / 2, this._hfuenftel),
            dttt: new vsdui_textoutput(id + '-dttt', this._anfang, this._anfang, this._bdrittel, this._hfuenftel).setstyle({
                'text-align': 'center'
            }),
            dtmm: new vsdui_textoutput(id + '-dtmm', this._anfang + this._bdrittel + 5, this._anfang, this._bdrittel, this._hfuenftel).setstyle({
                'text-align': 'center'
            }),
            dtjj: new vsdui_textoutput(id + '-dtjj', this._anfang + this._bdrittel + this._bdrittel + 10, this._anfang, this._bdrittel, this._hfuenftel).setstyle({
                'text-align': 'center'
            }),
            bttm: new vsdui_button(id + '-bttp', '+', this._anfang + this._bdrittel / 2, this._anfang + 1 * this._hfuenftel + 3, this._bdrittel / 2, this._hfuenftel),
            bmmm: new vsdui_button(id + '-bmmp', '+', this._anfang + this._bdrittel + 5 + this._bdrittel / 2, this._anfang + 1 * this._hfuenftel + 3, this._bdrittel / 2, this._hfuenftel),
            bjjm: new vsdui_button(id + '-bjjp', '+', this._anfang + this._bdrittel + this._bdrittel + 10 + this._bdrittel / 2, this._anfang + 1 * this._hfuenftel + 3, this._bdrittel / 2, this._hfuenftel),
            tagstring: new vsdui_label(id + '-tag', 'tag als string', this._anfang, this._anfang + 2 * this._hfuenftel + 9, this._cbreite, this._hfuenftel),
        }
        for (let item in this._comps) {
            this.getcontent().addcontent((this._comps[item].getcomponent()))
        }
        ;
        this._aktdate = new Date()
        let c = this
        let clevents = {
            bttp: function () {
                c._aktdate.setDate(c._aktdate.getDate() + 1)
                c.redraw()
            },
            bttm: function () {
                c._aktdate.setDate(c._aktdate.getDate() - 1)
                c.redraw()
            },
            bmmp: function () {
                c._aktdate.setMonth(c._aktdate.getMonth() + 1)
                c.redraw()
            },
            bmmm: function () {
                c._aktdate.setMonth(c._aktdate.getMonth() - 1)
                c.redraw()
            },
            bjjp: function () {
                c._aktdate.setFullYear(c._aktdate.getFullYear() + 1)
                c.redraw()
            },
            bjjm: function () {
                c._aktdate.setFullYear(c._aktdate.getFullYear() - 1)
                c.redraw()
            }

        }
        for (let eventitem in clevents) {
            vsevent.handle('click_' + id + '-' + eventitem, clevents[eventitem])
        }
        ;
        this.redraw()
    }
    /**
     *
     * nur interne verwendung
     * @returns {undefined}
     */
    redraw() {
        let x = this._comps
        x.dttt.setvalue(this._aktdate.getDate())
        x.dtmm.setvalue(this._aktdate.getMonth() + 1)
        x.dtjj.setvalue(this._aktdate.getFullYear())
        x.tagstring.addcontent(vsdate.getTagAsString(this._aktdate))
    }
    /**
     *
     * setzen des datums
     * @param {date} d
     * @returns {undefined}
     */
    setdate(d) {
        this._aktdate = d
        this.redraw()
    }
    getdate() {
        let x = this._comps

        return vsdate.formatDate(x.dttt.getvalue() + '.' + x.dtmm.getvalue() + '.' + x.dtjj.getvalue())

    }
}
