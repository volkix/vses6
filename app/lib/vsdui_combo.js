/*
 * (c) Volker Schulte volkerschulte@gmail.com
 * Änderungswünsche willkommen.
 *
 */

/**
* @module vsdui_combo
* @extends vsdui_base
*
*/

import vsdui_base from './vsdui_base.js'
import vsdom from './vsdom.js'
import vsevent from './vsevent.js'

export default class vsdui_combo extends vsdui_base {
  /**
   * @param {string} id
   * @param {arrobject} options
   * <pre>
   * [
   * {
   *  label : "hallo volker",
   *  value : "hv",
   *  selected : true
   * }
   * ]
   * </pre>
   * @param {number} x
   * @param {number} y
   * @param {number} w
   * @param {number} h
   *
   */
  constructor(id, options, x, y, w, h) {
    super(id, x, y, w, h, 'select')
    const o = {
      'background-color': 'white',
      'color': '#900000',
      'border': '1px solid silver'
    }
    this.that.size = 1
    this.setstyle(o)
    this.setoptions(options)
    this.that.addEventListener('change', e => {
      vsevent.send('changeselect_' + id, {
        id: id,
        value: ct.value
      })
    }, false)
  }
  /**
   * @param {arrobjects} val siehe beschreibung constructor
   * @returns {undefined}
   */
  setoptions(val) {

    vsdom.delallchilds(this.that)
    val.forEach(t => {
      if (t.selected && t.selected === true) {
        this.that.appendChild(vsdom.makeelem('option', {
          'value': '' + t.value,
          'selected': 'true'
        }, t.label))
      } else {
        this.that.appendChild(vsdom.makeelem('option', {
          'value': '' + t.value
        }, t.label))
      }
    })
  }
  /**
   * wird nur intern verwendet
   *
   * @param {type} arrSelect
   * @param {type} sItem
   * @returns {undefined}
   */
  _SelectItemInSelect(arrSelect, sItem) {

    arrSelect.options.forEach(element => {
      if (element.value === sItem) {
        element.selected = true
        return
      }
    });

  }

  /**
   *
   * @param {String} val value übergeben, der selektiert werden soll
   * @returns {undefined}
   */
  selectoption(val) {
    this._SelectItemInSelect(this.that, val)
  }
  /**
   *
   * @description gibt das label des gewählten eintrags zurück
   * @returns {vsdui_combo.getlabel.that.options.label|String}
   */
  getlabel() {

    this.that.options.forEach(element => {
      if (element.selected === true) {
        return element.label
      }
    });

    return ''
  }
  /**
   *
   * gibt den value des gewählten eintrags zurück
   * @returns {vsdui_combo.getvalue.that.options.value|String}
   */
  getvalue() {
    this.that.options.forEach(element => {
      if (element.selected === true) {
        return element.value
      }
    });
    return ''
  }
  ;
}
