/*
 * (c) Volker Schulte volkerschulte@gmail.com
 * Änderungswünsche willkommen.
 *
 */


/**
*
* @module vsdui_link
* @extends vsdui_base
*
*/
import vsdui_base from './vsdui_base.js'

export default class vsdui_link extends vsdui_base {
  /**
   * vsdui_link
   * html-link setzen
   *
   * @param {string} id
   * @param {string} href
   * @param {string} target
   * @param {string} text
   * @param {number} x
   * @param {number} y
   * @param {number} w
   * @param {number} h
   *
   */
  constructor(id, href, target, text, x, y, w, h) {
    super(id, x, y, w, h, 'a')
    let c = this.that
    c.setAttribute('href', href)
    c.setAttribute('target', target)
    this.addcontent(text)
    const o = {
      'background-color': '#0e7a0c',
      'color': 'white',
      'text-align': 'center',
      'cursor': 'pointer'
    }
    this.setstyle(o)

    let ct = this;
    ['mouseover', 'focus'].forEach(ee => {
      c.addEventListener(ee, e => {
        ct.setstyle({
          'background-color': 'orange'
        })
      })
    });
    ['mouseout', 'blur'].forEach(ee => {
      c.addEventListener(ee, e => {
        ct.setstyle({
          'background-color': '#0e7a0c'
        })
      })
    })
  }
  ;
}
