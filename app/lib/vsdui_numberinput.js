/*
 * (c) Volker Schulte volkerschulte@gmail.com
 * Änderungswünsche willkommen.
 *
 */

/**
*
* @module vsdui_numberinput
* @extends vsdui_textinput
*
*/

import vsdui_textinput from './vsdui_textinput.js'
import vsnumber from './vsnumber.js'

export default class vsdui_numberinput extends vsdui_textinput {
  /**
   *
   * vsdui_numberinput
   * eingabe eines numerischen wertes
   *
   * @param {string} id
   * @param {number} nk
   * @param {number} x
   * @param {number} y
   * @param {number} w
   * @param {number} h
   * @returns {vsdui_numberinput}
   */
  constructor(id, nk, x, y, w, h) {
    super(id, x, y, w, h)
    this.getcomponent().nk = nk
    this.setformatter(function (w) {
      return vsnumber.fz(w, nk)
    })
    this.setstyle({
      'text-align': 'right'
    })
  }
  /**
   *
   * ein numberwert wird formatiert in das feld übernommen
   * @param {number} n
   * @returns {undefined}
   */
  fromnumber(n) {
    this.setvalue(vsnumber.n2s(n, this.getcomponent().nk))
  }
  /**
   *
   * value als number
   * @returns {Number}
   */
  tonumber() {
    return vsnumber.s2n(this.getvalue(), this.getcomponent().nk)
  }
}
