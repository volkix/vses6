/*
 * (c) Volker Schulte volkerschulte@gmail.com
 * Änderungswünsche willkommen.
 *
 */

/**
* @module vsdui_blocker
* @extends vsdui_base
*/
import vsdui_base from './vsdui_base.js'

export default class vsdui_blocker extends vsdui_base {
  /**
   * @param {string} id
   * @param {string} zindex
   */
  constructor(id, zindex) {
    super(id, 0, 0, 10, 10)
    var o = {
      'background-color': 'orange',
      'opacity': '0.3',
      'z-index': zindex,
      'width': '100%',
      'height': '100%',
      'position': 'fixed',
      'top': '0px',
      'left': '0px'
    }
    this.setstyle(o)
    document.body.appendChild(this.that)
  }
}
