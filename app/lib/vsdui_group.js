/*
 * (c) Volker Schulte volkerschulte@gmail.com
 * Änderungswünsche willkommen.
 *
 */


/**
*
* @module vsdui_group
* @extends vsdui_base
*
*
*/

import vsdui_base from './vsdui_base.js'
import vsdom from './vsdom.js'

export class vsdui_group extends vsdui_base {
  /**
   * vsdui_group
   *
   * @param {string} id
   * @param {string} text gruppenüberschrift
   * @param {number} x
   * @param {number} y
   * @param {number} w
   * @param {number} h
   */
  constructor(id, text, x, y, w, h) {
    super(id, x, y, w, h, 'fieldset')
    const o = {
      'font-weight': 'bold',
      'background-color': 'white',
      'color': '#900000',
      'border': '1px solid',
      'overflow': 'auto'

    }
    this.setstyle(o)
    
    let c1 = vsdom.makeelem('legend', {
      id: id + '-legend'
    }, text)
    vscss.setstyle(c1, {
      'color': '#900000',
      'font-size': '16px',
      'background-color': 'white',
      'font-weight': 'bold',
      'margin-left': '3px'
    }
    )
    this.that.appendChild(c1)
  }
}
