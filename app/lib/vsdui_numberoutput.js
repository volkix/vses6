/*
 * (c) Volker Schulte volkerschulte@gmail.com
 * Änderungswünsche willkommen.
 *
 */

/**
*
* @module vsdui_numberoutput
* @extends vsdui_numberinput
*
*
*/

import vsdui_numberinput from './vsdui_numberinput.js'
import vsnumber from './vsnumber.js'

export default class vsdui_numberoutput extends vsdui_numberinput {
  /**
   * vsdui_numberoutput
   * das gleiche wie numberinput, aber ohne änderungsmöglichkeit
   *
   * @param {string} id
   * @param {number} nk
   * @param {number} x
   * @param {number} y
   * @param {number} w
   * @param {number} h
   * @returns {vsdui_numberoutput}
   */
  constructor(id, nk, x, y, w, h) {
    super(id, nk, x, y, w, h)
    this.getcomponent().nk = nk
    this.setformatter(function (w) {
      return vsnumber.fz(w, nk)
    })
    this.getcomponent().setAttribute('disabled', 'true')
    this.setstyle({
      'text-align': 'right'
    })
  }
}
