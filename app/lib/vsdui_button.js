/*
 * (c) Volker Schulte volkerschulte@gmail.com
 * Änderungswünsche willkommen.
 *
 */


/**
* @module vsdui_button
* @extends vsdui_base
* @see module:vsdui_base
*/

import vsevent from './vsevent.js'
import vsdui_base from './vsdui_base.js'

export default class vsdui_button extends vsdui_base {
  /**
   * @param {string} id
   * @param {string} text
   * @param {number} x
   * @param {number} y
   * @param {number} w
   * @param {number} h
   * @returns {vsdui_button}
   */

  constructor(id, text, x, y, w, h) {
    super(id, x, y, w, h, 'button')
    const o = {
      'background-color': '#0e7a0c',
      'color': 'white',
      'text-align': 'center',
      'cursor': 'pointer'
    }
    this.setstyle(o)
    this.addcontent(text)
    this.that.addEventListener('click', e => {
      vsevent.send('click_' + id, {
        'id': id,
        'e': e
      })
      vsevent.send('click', {
        'id': id,
        'e': e
      })
    }, false);

    ['mouseover', 'focus'].forEach(ee => {
      this.that.addEventListener(ee, e => {
        this.setstyle({
          'background-color': 'orange'
        })
      })
    });
    ['mouseout', 'blur'].forEach(ee => {
      this.that.addEventListener(ee, e => {
        this.setstyle({
          'background-color': '#0e7a0c'
        })
      })
    })
  }
}
