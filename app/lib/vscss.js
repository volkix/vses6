/*
 * (c) Volker Schulte volkerschulte@gmail.com
 * Änderungswünsche willkommen.
 *
 */
/**
 *
 * @module vscss
 *
 *
 */

export default class vscss {
    /**
     *
     *
     * @param {element} e domobjekt
     * @param {object} obj object mit den styleangaben
     */
  static setstyle (e, obj) {
    for (let v in obj) {
      try {
        e.style[v] = obj[v]
      } catch (exception) {
      }
    }
  }
    /**
     *
     *
     *
     * @param {element} e domobjekt
     * @param {object} obj object mit den styleangaben
     */
  static delstyle (e, obj) {
    for (let v in obj) {
      try {
        e.style[v] = ''
      } catch (exception) {
      }
    }
  }
    /**
     *
     *
     *
     * @param {Element} e
     * @param {String} clazz
     */
  static addclass (e, clazz) {
    if (!e.classList.contains(clazz)) {
      e.classList.add(clazz)
    }
  }
    /**
     *
     *
     *
     * @param {Element} e
     * @param {String} clazz
     */
  static removeclass (e, clazz) {
    e.classList.remove(clazz)
  }
    /**
     *
     *
     *
     * @param {Element} e
     * @param {String} clazz
     */
  static toggleclass (e, clazz) {
    e.classList.toggle(clazz)
  }
    /**
     *
     *
     *
     * @param {Element} e
     * @param {String} clazz
     */
  static setclass (e, clazz) {
    e.className = clazz
  }
}
