/*
 * (c) Volker Schulte volkerschulte@gmail.com
 * Änderungswünsche willkommen.
 *
 */

/**
 *
 * @module vsdui_window
 */
export default class vsdui_window {
  /**
   *
   * @returns {Number|Window.innerWidth}
   *  breite des fensters
   */
  static getwidth() {
    return window.innerWidth
  }
  /**
   *
   *  höhe des fensters
   * @returns {Window.innerHeight|Number}
   */
  static getheight() {
    return window.innerHeight
  }
  /**
   *
   *  pixel von dokumentstart nach unten
   * @returns {Number|Window.pageYOffset}
   */
  static getvonoben() {
    return window.pageYOffset
  }
  /**
   *
   *  pixel von dokumentstart links nach rechts
   * @returns {Number|Window.pageXOffset}
   */
  static getvonlinks() {
    return window.pageXOffset
  }
  /**
   *
   *  dokumentenhöhe
   * @returns {Number}
   */
  static getscrollheight() {
    return document.body.scrollHeight
  }
}
