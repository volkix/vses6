/*
 * (c) Volker Schulte volkerschulte@gmail.com
 * Änderungswünsche willkommen.
 *
 */

/**
*
* @module vsdui_timepicker
* @extends vsdui_panel
*
*/

import vsdui_panel from './vsdui_panel.js'
import vsdui_button from './vsdui_button.js'
import vsdui_textoutput from './vsdui_textoutput.js'

import vsdom from './vsdom.js'
import vsdate from './vsdate.js'
import vsevent from './vsevent.js'

export default class vsdui_timepicker extends vsdui_panel {
  /**
   * vsdui_timepicker
   * datumsauswahlbox
   * <pre>
   * sendet: 'timepicker' mit ID und VALUE
   * </pre>
   *
   *
   * @param {string} id
   * @param {string} ueberschrift
   * @param {number} x
   * @param {number} y
   * @param {number} w
   * @param {number} h
   */
  constructor(id, ueberschrift, x, y, w, h) {
    super(id, ueberschrift, x, y, w, h)
    this._cbreite = this.getcontent().getposition().w - 10
    this._choehe = this.getcontent().getposition().h - 10
    this._bdrittel = (this._cbreite - 10) / 3
    this._hfuenftel = (this._choehe - 9) / 5
    this._anfang = 5
    this._comps = {
      bhhp: new vsdui_button(id + '-bhhp', '+', this._anfang, this._anfang, this._bdrittel, this._hfuenftel),
      bmmp: new vsdui_button(id + '-bmmp', '+', this._anfang + this._bdrittel + 5, this._anfang, this._bdrittel, this._hfuenftel),
      bssp: new vsdui_button(id + '-bssp', '+', this._anfang + this._bdrittel + this._bdrittel + 10, this._anfang, this._bdrittel, this._hfuenftel),
      dthh: new vsdui_textoutput(id + '-dthh', this._anfang, this._anfang + 1 * this._hfuenftel + 3, this._bdrittel, this._hfuenftel).setstyle({
        'text-align': 'center'
      }),
      dtmm: new vsdui_textoutput(id + '-dtmm', this._anfang + this._bdrittel + 5, this._anfang + 1 * this._hfuenftel + 3, this._bdrittel, this._hfuenftel).setstyle({
        'text-align': 'center'
      }),
      dtss: new vsdui_textoutput(id + '-dtss', this._anfang + this._bdrittel + this._bdrittel + 10, this._anfang + 1 * this._hfuenftel + 3, this._bdrittel, this._hfuenftel).setstyle({
        'text-align': 'center'
      }),
      bhhm: new vsdui_button(id + '-bhhm', '-', this._anfang, this._anfang + 2 * this._hfuenftel + 6, this._bdrittel, this._hfuenftel),
      bmmm: new vsdui_button(id + '-bmmm', '-', this._anfang + this._bdrittel + 5, this._anfang + 2 * this._hfuenftel + 6, this._bdrittel, this._hfuenftel),
      bssm: new vsdui_button(id + '-bssm', '-', this._anfang + this._bdrittel + this._bdrittel + 10, this._anfang + 2 * this._hfuenftel + 6, this._bdrittel, this._hfuenftel),
      bja: new vsdui_button(id + '-bja', 'Ja', this._anfang, this._anfang + 4 * this._hfuenftel + 9, this._bdrittel, this._hfuenftel),
      bnein: new vsdui_button(id + '-bnein', 'Nein', this._anfang + this._bdrittel + 5, this._anfang + 4 * this._hfuenftel + 9, this._bdrittel, this._hfuenftel)
    }
    for (let item in this._comps) {
      this.getcontent().addcontent((this._comps[item].getcomponent()))
    }
    ;
    this._aktdate = new Date()
    let c = this
    let clevents = {
      bhhp: function () {
        c._aktdate.setHours(c._aktdate.getHours() + 1)
        c.redraw()
      },
      bhhm: function () {
        c._aktdate.setHours(c._aktdate.getHours() - 1)
        c.redraw()
      },
      bmmp: function () {
        c._aktdate.setMinutes(c._aktdate.getMinutes() + 1)
        c.redraw()
      },
      bmmm: function () {
        c._aktdate.setMinutes(c._aktdate.getMinutes() - 1)
        c.redraw()
      },
      bssp: function () {
        c._aktdate.setSeconds(c._aktdate.getSeconds() + 1)
        c.redraw()
      },
      bssm: function () {
        c._aktdate.setSeconds(c._aktdate.getSeconds() - 1)
        c.redraw()
      },
      bja: function () {
        let x = c._comps
        vsevent.send('timepicker_' + id,
          {
            id: id,
            value: vsdate.formatTime(c._aktdate).gesamt
          }
        )
        vsdom.delchild(vsdom.$(id).parentNode, vsdom.$(id))
      },
      bnein: function () {
        vsdom.delchild(vsdom.$(id).parentNode, vsdom.$(id))
      }
    }
    for (let eventitem in clevents) {
      vsevent.handle('click_' + id + '-' + eventitem, clevents[eventitem])
    }
    ;
    this.redraw()
  }
  /**
   *
   * nur interne verwendung
   * @returns {undefined}
   */
  redraw() {
    let x = this._comps
    x.dthh.setvalue(vsdate.formatTime(this._aktdate).stunde)
    x.dtmm.setvalue(vsdate.formatTime(this._aktdate).minute)
    x.dtss.setvalue(vsdate.formatTime(this._aktdate).sekunde)
  }
  /**
   *
   * setzen der gewünschten zeit
   * @param {time} d
   * @returns {undefined}
   */
  settime(d) {
    this._aktdate = d
    this.redraw()
  }
}
