/*
 * (c) Volker Schulte volkerschulte@gmail.com
 * Änderungswünsche willkommen.
 *
 */


/**
*
* @module vsdui_image
* @extends vsdui_base
*
*
*/

import vsdui_base from './vsdui_base.js'

export default class vsdui_image extends vsdui_base {
  /**
   * vsdui_image
   * bild komponente
   *
   * @param {string} id
   * @param {string} src pfad zum bild
   * @param {number} x
   * @param {number} y
   * @param {number} w
   * @param {number} h
   * @returns {vsdui_image}
   */
  constructor(id, src, x, y, w, h) {
    super(id, x, y, w, h, 'img')
   this.that.setAttribute('src', src)
  }
  ;
}
