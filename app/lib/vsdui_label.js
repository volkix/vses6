/*
 * (c) Volker Schulte volkerschulte@gmail.com
 * Änderungswünsche willkommen.
 *
 */


/**
*
* @module vsdui_label
* @extends vsdui_base
*
*/

import vsdui_base from './vsdui_base.js'

export default class vsdui_label extends vsdui_base {
  /**
   * vsdui_label
   * label komponente
   *
   * @param {type} id
   * @param {type} text
   * @param {type} x
   * @param {type} y
   * @param {type} w
   * @param {type} h
   * @returns {vsdui_label}
   */
  constructor(id, text, x, y, w, h) {
    super(id, x, y, w, h, 'button')
    const o = {
      'padding': '3px',
      'background-color': 'white',
      'color': '#900000',
      'text-align': 'left'
    }
    this.setstyle(o)
    this.addcontent(text)

    this.that.addEventListener('focus', e => {
      this.that.blur()
    }, false)
  }
}
