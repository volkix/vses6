/*
 * (c) Volker Schulte volkerschulte@gmail.com
 * Änderungswünsche willkommen.
 *
 */

/**
 *
 * @module vsevent
 */
export default class vsevent {
    /**
     *  loeschen der channels
     * @returns {nothing}
     */
    static clear_channels() {
        vsevent.channels = {}
    }
    /**
     *
     *  senden eines events
     * @param {String} Kanal
     * @param {Object} variablen
     * @returns {nothing}
     */
    static send(kanal, ...args) {
        if (!vsevent.channels) {
            vsevent.channels = {}
        }
        try {
            console.log(kanal, JSON.stringify(args), ...args)

        } catch (e) {
        }
        try {
            let cbs = vsevent.channels[kanal];
            if (cbs) {
                setTimeout(() => {
                    cbs
                        .forEach(cb => {
                            cb(...args);
                        });

                }, 1);
            }
            else{
                console.log(`kanal ${kanal} nicht gefunden`)
            }

        } catch (error) {
            //ignorieren, da nur kein listener für den kanal

        }


    }
    /**
     *
     *  funktion zur behandlung eines events definieren
     * @param {string} channel
     * @param {function} callback
     * @param { boolean} multi
     * @returns {nothing}
     */
    static handle(channel, callback, multi /* boolean */) {
        let m = multi || false;

        if (!vsevent.channels) {
            vsevent.channels = {}
        }

        if ((!vsevent.channels[channel]) || (!m)) {
            vsevent.channels[channel] = []
        }
        vsevent.channels[channel].push(callback)
    }
    /**
     *
     *  alle eventkanäle zurückgeben
     * @returns {vsevent.channels}
     */
    static getchannels() {
        return vsevent.channels
    }
}
