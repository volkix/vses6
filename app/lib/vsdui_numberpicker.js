/*
 * (c) Volker Schulte volkerschulte@gmail.com
 * Änderungswünsche willkommen.
 *
 */

/**
*
* @module vsdui_numberpicker
* @extends vsdui_panel
*
*
*/

import vsdui_panel from './vsdui_panel.js'
import vsdui_button from './vsdui_button.js'
import vsdui_numberoutput from './vsdui_numberoutput.js'

import vsdom from './vsdom.js'
import vsnumber from './vsnumber.js'
import vsevent from './vsevent.js'

export default class vsdui_numberpicker extends vsdui_panel {
  /**
   * vsdui_numberpicker
   * nummernfeld
   * <pre>
   * sendet: 'numberpicker' mit ID und VALUE und STRVALUE
   * </pre>
   *
   * @param {string} id
   * @param {string} ueberschrift
   * @param {number} x
   * @param {number} y
   * @param {number} w
   * @param {number} h
   */
  constructor(id, ueberschrift, x, y, w, h) {
    super(id, ueberschrift, x, y, w, h)
    this._cbreite = this.getcontent().getposition().w - 10
    this._choehe = this.getcontent().getposition().h - 10
    this._bviertel = (this._cbreite - 10) / 4
    this._hfuenftel = (this._choehe - 8) / 5
    this._anfang = 5
    this._comps = {
      ntext: new vsdui_numberoutput(id + '-ntext', 2, this._anfang, this._anfang, this._cbreite, this._hfuenftel - 2),
      b1: new vsdui_button(id + '-b1', '1', this._anfang, this._anfang + 1 * this._hfuenftel, this._bviertel, this._hfuenftel - 2),
      b2: new vsdui_button(id + '-b2', '2', this._anfang + 1 * this._bviertel + 3, this._anfang + 1 * this._hfuenftel, this._bviertel, this._hfuenftel - 2),
      b3: new vsdui_button(id + '-b3', '3', this._anfang + 2 * this._bviertel + 6, this._anfang + 1 * this._hfuenftel, this._bviertel, this._hfuenftel - 2),
      b4: new vsdui_button(id + '-b4', '4', this._anfang + 3 * this._bviertel + 9, this._anfang + 1 * this._hfuenftel, this._bviertel, this._hfuenftel - 2),

      b5: new vsdui_button(id + '-b5', '5', this._anfang, this._anfang + 2 * this._hfuenftel, this._bviertel, this._hfuenftel - 2),
      b6: new vsdui_button(id + '-b6', '6', this._anfang + 1 * this._bviertel + 3, this._anfang + 2 * this._hfuenftel, this._bviertel, this._hfuenftel - 2),
      b7: new vsdui_button(id + '-b7', '7', this._anfang + 2 * this._bviertel + 6, this._anfang + 2 * this._hfuenftel, this._bviertel, this._hfuenftel - 2),
      b8: new vsdui_button(id + '-b8', '8', this._anfang + 3 * this._bviertel + 9, this._anfang + 2 * this._hfuenftel, this._bviertel, this._hfuenftel - 2),

      b9: new vsdui_button(id + '-b9', '9', this._anfang, this._anfang + 3 * this._hfuenftel, this._bviertel, this._hfuenftel - 2),
      b0: new vsdui_button(id + '-b0', '0', this._anfang + 1 * this._bviertel + 3, this._anfang + 3 * this._hfuenftel, this._bviertel, this._hfuenftel - 2),
      bk: new vsdui_button(id + '-bk', ',', this._anfang + 2 * this._bviertel + 6, this._anfang + 3 * this._hfuenftel, this._bviertel, this._hfuenftel - 2),
      bc: new vsdui_button(id + '-bc', 'C', this._anfang + 3 * this._bviertel + 9, this._anfang + 3 * this._hfuenftel, this._bviertel, this._hfuenftel - 2),

      bja: new vsdui_button(id + '-bja', 'Ja', this._anfang, this._anfang + 4 * this._hfuenftel, this._bviertel, this._hfuenftel - 2),
      bnein: new vsdui_button(id + '-bnein', 'Nein', this._anfang + 1 * this._bviertel + 3, this._anfang + 4 * this._hfuenftel, this._bviertel, this._hfuenftel - 2),
      bb: new vsdui_button(id + '-bb', '<<', this._anfang + 3 * this._bviertel + 9, this._anfang + 4 * this._hfuenftel, this._bviertel, this._hfuenftel - 2)

    }
    for (let item in this._comps) {
      this.getcontent().addcontent((this._comps[item].getcomponent()))
    }
    ;
    let c = this
    let loe = function () {
      if (c._comps.ntext.tonumber() === 0.00) {
        c._comps.ntext.setvalue('')
      }
    }
    let clevents = {
      bc: function () {
        c._comps.ntext.setvalue('')
      },
      b1: function () {
        c._comps.ntext.setvalue(c._comps.ntext.getvalue() + '1')
      },
      b2: function () {
        c._comps.ntext.setvalue(c._comps.ntext.getvalue() + '2')
      },
      b3: function () {
        c._comps.ntext.setvalue(c._comps.ntext.getvalue() + '3')
      },
      b4: function () {
        c._comps.ntext.setvalue(c._comps.ntext.getvalue() + '4')
      },
      b5: function () {
        c._comps.ntext.setvalue(c._comps.ntext.getvalue() + '5')
      },
      b6: function () {
        c._comps.ntext.setvalue(c._comps.ntext.getvalue() + '6')
      },
      b7: function () {
        c._comps.ntext.setvalue(c._comps.ntext.getvalue() + '7')
      },
      b8: function () {
        c._comps.ntext.setvalue(c._comps.ntext.getvalue() + '8')
      },
      b9: function () {
        c._comps.ntext.setvalue(c._comps.ntext.getvalue() + '9')
      },
      b0: function () {
        c._comps.ntext.setvalue(c._comps.ntext.getvalue() + '0')
      },
      bk: function () {
        c._comps.ntext.setvalue(c._comps.ntext.getvalue().replace(',', '') + ',')
      },
      bb: function () {
        try {
          c._comps.ntext.setvalue(c._comps.ntext.getvalue().substr(0, c._comps.ntext.getvalue().length - 1))
        } catch (e) {
        }
      },
      bja: function () {
        let x = c._comps
        vsevent.send('numberpicker_' + id,
          {
            id: id,
            value: x.ntext.tonumber(),
            strvalue: vsnumber.fz(x.ntext.getvalue(), 2)
          }
        )
        vsdom.delchild(vsdom.$(id).parentNode, vsdom.$(id))
      },
      bnein: function () {
        vsdom.delchild(vsdom.$(id).parentNode, vsdom.$(id))
      }
    }
    for (let eventitem in clevents) {
      vsevent.handle('click_' + id + '-' + eventitem, clevents[eventitem])
    }
    ;
  }
  /**
   *
   * setzen der zahl aus einem numerischen wert
   * @param {number} n
   * @returns {undefined}
   */
  setvalue(n) {
    this._comps['ntext'].fromnumber(n)
  }
  /**
   *
   * setzen der zahl aus einem alphanumerischen wert, der in einem string steht
   * @param {string} n
   * @returns {undefined}
   */
  setstrvalue(n) {
    this._comps['ntext'].fromnumber(vsnumber.s2n(n, 2))
  }
}
