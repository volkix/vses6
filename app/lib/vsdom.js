/*
 * (c) Volker Schulte volkerschulte@gmail.com
 * Änderungswünsche willkommen.
 *
 */

/**
 *
 *
 * @module vsdom
 *
 *
 */
/** @class vsdom */
export default class vsdom {
    /**
     *
     *  holen eines elementes mittels der id
     * @param {String} id
     * @returns {Element}
     */
  static $ (id) {
    return document.getElementById(id)
  }
    /**
     *
     *
     *  holen von elementen
     * @param {String} selector
     * @param {Element} startelement
     * @returns {array}
     */
  static $$ (selector, startelement = document) {
    
    return [].slice.call(startelement.querySelectorAll(selector))
  }

    /**
     *
     *  vsdom
     *  erstellt ein Element ggf. mit attributen
     * @param {string} elem  Bezeichnung des Tags
     * @param {object} att  name:value objekt ggf style unterobjekt zum setzen von attributen
     * @param {string|element} text (optional) innerHTML|appendchild wird gesetzt
     * @return {Element} neues DomElement
     *
     *
     */
  static makeelem (elem, att, text) {
    let newelem = document.createElement(elem)
    if (att) {
      for (let v in att) {
        if (v === 'style') {
          let ss = att[v]
          for (let vv in ss) {
            try {
              newelem.style[vv] = ss[vv]
            } catch (exception) {

            }
          }
        } else {
          newelem.setAttribute(v, att[v])
        }
      }
    }
    if (text) {
      if (text instanceof Element) {
        newelem.appendChild(text)
      } else {
        newelem.innerHTML = text
      }
    }
    return newelem
  }
    /**
     *
     *  vsdom
     *  setzen von attributen
     * @param {Element} elem
     * @param {object} attr
     * @returns {void}
     */
  static setattr (elem, attr) {
    for (let v in attr) {
      elem.setAttribute(v, attr[v])
    }
  }
    /**
     *
     *  vsdom
     *  lesen eines attributes
     * @param {Element} elem
     * @param {String} attr
     * @returns {String}
     */
  static getattr (elem, attr) {
    return elem.getAttribute(attr)
  }
    /**
     *
     *  vsdom
     *  entfernen von attributen
     * @param {element} elem
     * @param {array} attr
     * @returns {void}
     */
  static delattr (elem, attr) {
    attr.forEach(el => elem.removeAttribute(el));
  }
    /**
     *
     *
     *  vsdom
     *  löscht ein Element
     * @param {element} elem : element, von dem ein Kindelement gelöscht werden soll
     * @param {element} child : das zu löschende Element als Node!
     *
     */
  static delchild (elem, child) {
    elem.removeChild(child)
  }
    /**
     *
     *  vsdom
     *  löscht alle unterelemente
     * @param {Element} element
     * @returns {void}
     */
  static delallchilds (element) {
    try {
      while (element.firstChild) {
        element.removeChild(element.firstChild)
      }
    } catch (e) {

    }
  }
    /**
     *
     *
     *  vsdom
     *  append child
     * @param {element} elem : element, bei dem ein Kindelement angefügt werden soll
     * @param {element} child : das anzufügende Element als Node!
     *
     *
     */
  static appchild (elem, child) {
    elem.appendChild(child)
  }
    /**
     *
     *
     *  vsdom
     *  einfügen Element
     * @param {element} elem : element, bei dem ein Kinelement angefügt werden soll
     * @param {element} child : das anzufügende Element als Node!
     * @param {element} beforechild : vor diesem Element soll eingefügt werden!
     *
     */
  static inschildbefore (elem, child, beforechild) {
    elem.insertBefore(child, beforechild)
  }
    /**
     *
     * [5~
     *  vsdom
     *  einfügen Element
     * @param {element} elem : element, bei dem ein Kinelement angefügt werden soll
     * @param {element} child : das anzufügende Element als Node!
     * @param {element} afterchild : nach diesem Element soll eingefügt werden!
     *
     */
  static inschildafter (elem, child, afterchild) {
    elem.insertBefore(child, afterchild.nextSibling)
  }
}
