/*
 * (c) Volker Schulte volkerschulte@gmail.com
 * Änderungswünsche willkommen.
 *
 */


/**
 *
 * @module vsdui_grid
 * @extends vsdui_base
 *
 *
 */

import vsdui_base from './vsdui_base.js'
import vsdom from './vsdom.js'
import vscss from './vscss.js'

export default class vsdui_grid extends vsdui_base {
    /**
     * grid komponente
     *
     * @param {string} id
     * @param {number} x
     * @param {number} y
     * @param {number} w
     * @param {number} h
     */
    constructor(id, x, y, w, h) {
        super(id, x, y, w, h, 'div')
        let o = {
            'border': '0px solid #e0e0e0',
            'overflow': 'auto'
        }
        this.setstyle(o)
        this._id = id
        this._zeilenhoehe = 30
        // let c = this.that;
        let cont = new vsdui_base(id + '-con', 1, 1, w - 2, h - 2)
        this._cont = cont
        this._printheader = true
        cont.setstyle({
            'background-color': 'white',
            'color': '#900000',
            'border': '0px solid',
            'overflow': 'auto'
        })
        this.addcontent(cont.getcomponent())
    }
    /**
     *
     * gibt die content-komponente zurück
     * @returns {vsdui_grid._cont}
     */
    getcontent() {
        return this._cont
    }
    /**
     *
     * @description
     * <pre>
     * [
     * {
     *  title : "meindatenfeld",
     *  ertign : "center",
     *  width : 100,
     *  bind : function(datenzeile){
     *      return datenzeile.datenfeld;
     *  }
     * }
     * ]
     * </pre>
     * @param {arrobjects} b
     * @returns {vsdui_grid}
     */
    setcolstyleandbindings(b) {

        this._colbindings = b
        return this
    }
    /**
     *
     * @param {arrobjects} d
     * @description daten-objekte
     * <pre>
     * [
     * {
     * nachname : "schulte",
     * vorname : "volker"
     * }
     * ]
     * </pre>
     * @returns {vsdui_grid}
     */
    setcoldata(d) {
        this._coldata = d
        return this
    }
    /**
     *
     * rückgabe der grid-daten
     * @returns {arrayofobjects}
     */
    getcoldata() {
        return this._coldata
    }
    /**
     *
     * festlegen der zeilenhöhe der daten
     * @param {number} zh
     * @returns {vsdui_grid}
     */
    setzeilenhoehe(zh) {
        this._zeilenhoehe = zh
        return this
    }
    /**
     *
     * sollen die spaltenüberschriften mit ausgegeben werden
     * @param {boolean} ph
     * @returns {vsdui_grid}
     */
    setprintheader(ph) {
        this._printheader = ph
        return this
    }

    /**
     * highlighten der aktuellen zeile?
     * @param {boolean} hr 
     */
    sethighlightrow(hr) {
        this._rowhighlight = hr
        return this
    }
    /**
     *
     * erstellen des grid-dom-objektes
     * @returns {undefined}
     */
    render() {
        // alte daten löschen
        vsdom.delallchilds(this.getcontent().getcomponent())
        // neue Daten schreiben
        let breite = 333;//this.getposition().w
        let hoe = 26
        let vertical_begin = hoe
       
        if (this._printheader) {
            // header bauen
            this.header = new vsdui_base(this._id + '-header', 0, 0, breite - 4, hoe)
            this.header.setstyle({
                'background-color': 'silver',
                'font-weight': 'bold',
                'color': '#900000',
                'border': '0px solid #e0e0e0'
            })
            let anz = this._colbindings.length
            let anfang = 0
            for (let i = 0, max = anz;
                i < max; i++) {
                let c = this._colbindings[i]
                let cc = new vsdui_base(this._id + '-ch-' + i, anfang, 1, c.width, hoe - 4)
                cc.setstyle({
                    'background-color': '#e0e0e0',
                    'font-weight': 'bold',
                    'color': '#900000',
                    'border-right': '1px solid silver',
                    'overflow': 'hidden',
                    'text-align': c.align || 'center'
                })
                cc.addcontent(c.title)
                anfang += c.width
                this.header.getcomponent().appendChild(cc.getcomponent())
            }
            ;
            this.header.setposition({
                w: anfang + 1
            })
            this.getcontent().getcomponent().appendChild(this.header.getcomponent())
        } else {
            vertical_begin = 0
        }
        // jetzt die Datensätze

        let anzds = this._coldata.length
        let anzdscol = this._colbindings.length
        let anfang = 0
        let cd = this._coldata
        let tid = this._id
        hoe = this._zeilenhoehe
        for (let ii = 0; ii < anzds; ii++) {
            let odd = ((ii % 2) === 0)
            let zeilecol = '#f0f0f0'
            if (odd) {
                zeilecol = 'white'
            }
            let zeile = new vsdui_base(tid + '-zeile-' + ii, 0, vertical_begin, breite - 4, hoe)
            zeile.setstyle({
                'background-color': zeilecol
            })
            this.getcontent().getcomponent().appendChild(zeile.getcomponent());

            if (this._rowhighlight == true) {
                ['mouseover', 'focus'].forEach(ee => {
                    vsdom.$(tid + '-zeile-' + ii).addEventListener(ee, e => {
                        vscss.setstyle(vsdom.$(tid + '-zeile-' + ii), {
                            'background-color': 'lightblue'
                        })
                    })
                });
                ['mouseout', 'blur'].forEach(ee => {
                    vsdom.$(tid + '-zeile-' + ii).addEventListener(ee, e => {
                        vscss.setstyle(vsdom.$(tid + '-zeile-' + ii), {
                            'background-color': vsdom.$(tid + '-zeile-' + ii)._style['background-color']
                        })
                    })
                })

            }

            anfang = 0;
            for (let ids = 0; ids < anzdscol; ids++) {
                let c = this._colbindings[ids]
                let cc = new vsdui_base(this._id + '-cz-' + ii + '-cs-' + ids, anfang, 0, c.width, hoe)
                cc.setstyle({
                    'background-color': 'transparent',
                    'color': '#900000',
                    'border-right': '0px solid',
                    'overflow': 'hidden',
                    'text-align': c.align || 'center'
                })
                try {
                    cc.addcontent(c.bind.call(cd[ii], cd[ii], ii))
                } catch (e) {

                }
                anfang += c.width
                zeile.getcomponent().appendChild(cc.getcomponent())
            }
            zeile.setposition({
                w: anfang + 1
            })
            vertical_begin += hoe
        }
    }
}
