/*
 * (c) Volker Schulte volkerschulte@gmail.com
 * Änderungswünsche willkommen.
 *
 */

/**
*
* @module vsdui_dateoutput
* @extends vsdui_dateinput
*
*
*/

import vsdui_dateinput from './vsdui_dateinput.js'

export default class vsdui_dateoutput extends vsdui_dateinput {
  /**
   * vsdui_dateoutput
   * datumsausgabe
   *
   * @param {string} id
   * @param {number} x
   * @param {number} y
   * @param {number} w
   * @param {number} h
   * @returns {vsdui_dateoutput}
   */
  constructor(id, x, y, w, h) {
    super(id, x, y, w, h)
    this.getcomponent().setAttribute('readonly', 'true')
  }
}
