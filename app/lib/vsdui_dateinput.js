/*
 * (c) Volker Schulte volkerschulte@gmail.com
 * Änderungswünsche willkommen.
 *
 */


/**
 * @module vsdui_dateinput
 * @extends vsdui_textinput
 *
 *
 */

import vsdui_textinput from './vsdui_textinput.js'
import vsdate from './vsdate.js'
import vsevent from './vsevent.js'
import dp from './vsdui_datepicker.js'

export default class vsdui_dateinput extends vsdui_textinput {
    /**
     *
     * dateinput-feld
     *
     * @param {string} id
     * @param {number} x
     * @param {number} y
     * @param {number} w
     * @param {number} h
     * @returns {vsdui_dateinput}
     */
    constructor(id, x, y, w, h) {
        super(id, x, y, w, h)
        this.setformatter(vsdate.formatDate)
        this.setstyle({
            'text-align': 'center'
        })

    }
    getdate() {
 
        return vsdate.formatDate(this.getvalue())
    }

    setdatepicker() {
        var c = this;
        
        var id = c.getcomponent().id;
        c.setvalue(vsdate.heute());
        var p = c.getposition();
        this.getcomponent().addEventListener("focus", function (e) {
            c.dp = new dp(id + "-dp", "datum", p.x, p.y + p.h, p.w, 200);

            c.dp.appendtodom(document.body)
            c.dp.setdate(vsdate.parseDate(c.getvalue()))
        });

        vsevent.handle("datepicker_" + id + "-dp", function (d) {
            try {
                c.setvalue(d.value)
            } catch (e) {
                console.log(e)
            }


        });
        return this;
    }
}
