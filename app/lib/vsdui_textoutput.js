/*
 * (c) Volker Schulte volkerschulte@gmail.com
 * Änderungswünsche willkommen.
 *
 */


/**
*
* @module vsdui_textoutput
* @extends vsdui_textinput
*
*/

import vsdui_textinput from './vsdui_textinput.js'

export default class vsdui_textoutput extends vsdui_textinput {
  /**
   * vsdui_textoutput
   * das gleiche wie textinput, aber nicht änderbar
   *
   * @param {string} id
   * @param {number} x
   * @param {number} y
   * @param {number} w
   * @param {number} h
   * @returns {vsdui_textoutput}
   */
  constructor(id, x, y, w, h) {
    super(id, x, y, w, h)
    this.getcomponent().setAttribute('disabled', 'true')
  }
}
