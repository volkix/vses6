import vsevent from '../lib/vsevent.js';
import vsdate from '../lib/vsdate.js';
import vsdom from '../lib/vsdom.js';
import notizen from './notizen.js';

export default class events {

    static makeEvents() {


        vsevent.handle("click", e => {
            if (e.id.startsWith("del_")) {
                let ret = notizen.removeNotizen(e.id.substr(4))
                if (ret.includes("error")) {
                    alert(ret);
                    return;
                }
                vs.comp.twas.setvalue("")
                vsevent.send("click_bsuchen", {})
            }
            if (e.id.startsWith("upd_")) {
                let _id = e.id.substr(4);
                let ret = notizen.updateNotizen(_id, vsdom.$("to_" + _id).value, vsdom.$("ti_" + _id).value)
                if (ret.includes("error")) {
                    alert(ret);
                    return;
                }

                vsevent.send("click_bsuchen", {})

            }
        });
        vsevent.handle("click_bsuchen", () => {
            let suche = "";

            if (vs.comp.twas.getvalue() == "") {
                suche = vsdate.ddpmmpyyyy2yyyymmdd(vs.comp.twann.getdate())
            } else {
                suche = vs.comp.twas.getvalue()
            }
            let griddata = [];
            let arrnotiz = JSON.parse(notizen.getNotizen(suche + "*"))["data"];
            arrnotiz.forEach(element => {
              let arrp = element.daten.split("::");
                griddata.push({
                    id: element.rowid,
                    wann: arrp[0],
                    was: arrp[1]
                })
               
            });
            vs.comp.tgrid.setcoldata(griddata);
            vs.comp.tgrid.render()

        });
        vsevent.handle("click_baza", () =>{
            let jetzt = vsdate.formatTime(new Date()).gesamt
            let ret = notizen.putNotizen(vs.comp.twann.getdate(), "aza " + jetzt)

            if (ret.includes("error")) {
                alert(ret);
                return;
            }
            vs.comp.twas.setvalue("")
            vsevent.send("click_bsuchen", {})
        });
        vsevent.handle("click_baze", function () {
            let jetzt = vsdate.formatTime(new Date()).gesamt
            let ret = notizen.putNotizen(vs.comp.twann.getdate(), "aze " + jetzt)

            if (ret.includes("error")) {
                alert(ret);
                return;
            }
            vs.comp.twas.setvalue("")
            vsevent.send("click_bsuchen", {})
        });
        vsevent.handle("click_bneu", function () {
            let ret = notizen.putNotizen(vs.comp.twann.getdate(), vs.comp.twas.getvalue())

            if (ret.includes("error")) {
                alert(ret);
                return;
            }
            vs.comp.twas.setvalue("")
            vsevent.send("click_bsuchen", {})
        });


    }

}

