(function () {
  'use strict';

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  function _createClass(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties(Constructor, staticProps);
    return Constructor;
  }

  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function");
    }

    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        writable: true,
        configurable: true
      }
    });
    if (superClass) _setPrototypeOf(subClass, superClass);
  }

  function _getPrototypeOf(o) {
    _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
      return o.__proto__ || Object.getPrototypeOf(o);
    };
    return _getPrototypeOf(o);
  }

  function _setPrototypeOf(o, p) {
    _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
      o.__proto__ = p;
      return o;
    };

    return _setPrototypeOf(o, p);
  }

  function _assertThisInitialized(self) {
    if (self === void 0) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }

    return self;
  }

  function _possibleConstructorReturn(self, call) {
    if (call && (typeof call === "object" || typeof call === "function")) {
      return call;
    }

    return _assertThisInitialized(self);
  }

  /*
   * (c) Volker Schulte volkerschulte@gmail.com
   * Änderungswünsche willkommen.
   *
   */ /**
   *
   * @module vsevent
   */var vsevent=/*#__PURE__*/function(){function a(){_classCallCheck(this,a);}return _createClass(a,null,[{key:"clear_channels",/**
       *  loeschen der channels
       * @returns {nothing}
       */value:function clear_channels(){a.channels={};}/**
       *
       *  senden eines events
       * @param {String} Kanal
       * @param {Object} variablen
       * @returns {nothing}
       */},{key:"send",value:function send(b){for(var c=arguments.length,d=Array(1<c?c-1:0),e=1;e<c;e++)d[e-1]=arguments[e];a.channels||(a.channels={});try{var f;(f=console).log.apply(f,[b,JSON.stringify(d)].concat(d));}catch(a){}try{var g=a.channels[b];g?setTimeout(function(){g.forEach(function(a){a.apply(void 0,d);});},1):console.log("kanal ".concat(b," nicht gefunden"));}catch(a){//ignorieren, da nur kein listener für den kanal
  }}/**
       *
       *  funktion zur behandlung eines events definieren
       * @param {string} channel
       * @param {function} callback
       * @param { boolean} multi
       * @returns {nothing}
       */},{key:"handle",value:function handle(b,c,d/* boolean */){a.channels||(a.channels={}),a.channels[b]&&(d||!1)||(a.channels[b]=[]),a.channels[b].push(c);}/**
       *
       *  alle eventkanäle zurückgeben
       * @returns {vsevent.channels}
       */},{key:"getchannels",value:function getchannels(){return a.channels}}]),a}();

  /*
   * (c) Volker Schulte volkerschulte@gmail.com
   * Änderungswünsche willkommen.
   *
   */ /**
   *
   * @module vsdui_window
   */var vsdui_window=/*#__PURE__*/function(){function a(){_classCallCheck(this,a);}return _createClass(a,null,[{key:"getwidth",/**
     *
     * @returns {Number|Window.innerWidth}
     *  breite des fensters
     */value:function getwidth(){return window.innerWidth}/**
     *
     *  höhe des fensters
     * @returns {Window.innerHeight|Number}
     */},{key:"getheight",value:function getheight(){return window.innerHeight}/**
     *
     *  pixel von dokumentstart nach unten
     * @returns {Number|Window.pageYOffset}
     */},{key:"getvonoben",value:function getvonoben(){return window.pageYOffset}/**
     *
     *  pixel von dokumentstart links nach rechts
     * @returns {Number|Window.pageXOffset}
     */},{key:"getvonlinks",value:function getvonlinks(){return window.pageXOffset}/**
     *
     *  dokumentenhöhe
     * @returns {Number}
     */},{key:"getscrollheight",value:function getscrollheight(){return document.body.scrollHeight}}]),a}();

  /** class vsdui_base */var vsdui_base=/*#__PURE__*/function(){/**
       *
       * @param {string} id
       * @param {number} x
       * @param {number} y
       * @param {number} w
       * @param {number} h
       * @param {string} elem
       * @returns {vsdui_base}
       */function a(b,c,d,e,f,g){_classCallCheck(this,a),this.that=document.createElement(g||"div"),this.that._id=b,this.that._x=c,this.that._y=d,this.that._w=e,this.that._h=f,this.that._position="absolute",this.that._style={"box-sizing":"border-box","font-family":"Monaco, monospace","background-color":"white",color:"#900000","font-size":"16px",border:"0px solid",padding:"0px",margin:"0px"},this.that._content="",this._render();}/**
       * @returns {vsdui_base}
       */return _createClass(a,[{key:"_render",value:function _render(){try{return this.that.id=this.that._id,this.setposition(this.getposition()),this.setstyle(this.that._style),this.addcontent(this.that._content),this}catch(a){console.log(a);}}/**
       *
       *  objekt mit css-styles übergeben
       * @param {Object} obj
       * @returns {vsdui_base}
       */},{key:"setstyle",value:function setstyle(a){for(var b in a)try{this.that._style[b]=a[b];}catch(a){console.log(a);}for(var c in this.that._style)try{this.that.style[c]=this.that._style[c];}catch(a){console.log(a);}return this}/**
       *
       *
       *  position setzen
       * @param {object} o
       * @returns {vsdui_base}
       */},{key:"setposition",value:function setposition(a){return this.that._x=a.x||this.that._x,this.that._y=a.y||this.that._y,this.that._w=a.w||this.that._w,this.that._h=a.h||this.that._h,this.that._position="absolute",this.that.style.position=this.that._position,this.that.style.top=this.that._y+"px",this.that.style.left=this.that._x+"px",this.that.style.width=this.that._w+"px",this.that.style.height=this.that._h+"px",this}/**
       *
       *  holt die position der komponente
       * @returns {vsdui_base.getposition.o}
       */},{key:"getposition",value:function getposition(){var a={x:this.that._x,y:this.that._y,w:this.that._w,h:this.that._h};return a}/**
       *
       *  der name sagt alles
       * @returns {vsdui_base}
       */},{key:"center_horizontal",value:function center_horizontal(a){var c=vsdui_window.getwidth()/2,b=vsdui_window.getvonlinks()+(c-this.getposition().w/2);//parentelement ist belegt
  if(a&&null!=a)// this ist childnode von p
  if(a.getcomponent().contains(this.getcomponent())){var d=a.getposition().w/2;b=d-this.getposition().w/2;}else{//this ist nicht childnode von p, soll aber trotzdem zentriert werden
  var e=a.getposition().w/2;b=a.getposition().x+e-this.getposition().w/2;}return this.setposition({x:b}),this}/**
       *
       *  der name sagt alles
       * @returns {vsdui_base}
       */},{key:"center_vertical",value:function center_vertical(a){var b=vsdui_window.getheight()/2,c=vsdui_window.getvonoben()+(b-this.getposition().h/2);return a&&null!=a&&(a.getcomponent().contains(this.getcomponent())?(b=a.getposition().h/2,c=b-this.getposition().h/2):(b=a.getposition().h/2,c=a.getposition().y+b-this.getposition().h/2)),this.setposition({y:c}),this}/**
       *
       *  der name sagt alles
       * @returns {vsdui_base}
       */},{key:"center_both",value:function center_both(a){return this.center_horizontal(a),this.center_vertical(a),this}/**
       *
       *  childnodes bzw. innerhtml hinzufügen
       * @param {Element|String} c
       * @returns {vsdui_base}
       */},{key:"addcontent",value:function addcontent(a){return this.that._content=a,this.that._content instanceof Element?this.that.appendChild(this.that._content):this.that.innerHTML=this.that._content,this.that._content=this.that.innerHTML,this}/**
       *
       *  das eigentliche domobjekt zurückgeben
       * @returns {Element}
       */},{key:"getcomponent",value:function getcomponent(){return this.that}/**
       *
       *  die komponente aus dem dom entfernen
       * @returns {undefined}
       */},{key:"removefromdom",value:function removefromdom(){var a=this.that;a.parentNode.removeChild(a);}/**
       *
       * die komponente in das dom einfügen
       * @param {element} elem
       * @returns {vsdui_base}
       */},{key:"appendtodom",value:function appendtodom(a){return a.appendChild(this.that),this}}]),a}();

  var vsdui_button=/*#__PURE__*/function(a){/**
     * @param {string} id
     * @param {string} text
     * @param {number} x
     * @param {number} y
     * @param {number} w
     * @param {number} h
     * @returns {vsdui_button}
     */function b(a,c,d,e,f,g){var h;_classCallCheck(this,b),h=_possibleConstructorReturn(this,_getPrototypeOf(b).call(this,a,d,e,f,g,"button"));return h.setstyle({"background-color":"#0e7a0c",color:"white","text-align":"center",cursor:"pointer"}),h.addcontent(c),h.that.addEventListener("click",function(b){vsevent.send("click_"+a,{id:a,e:b}),vsevent.send("click",{id:a,e:b});},!1),["mouseover","focus"].forEach(function(a){h.that.addEventListener(a,function(){h.setstyle({"background-color":"orange"});});}),["mouseout","blur"].forEach(function(a){h.that.addEventListener(a,function(){h.setstyle({"background-color":"#0e7a0c"});});}),h}return _inherits(b,a),b}(vsdui_base);

  var vsdui_textinput=/*#__PURE__*/function(a){/**
       * vsdui_textinput
       *
       * @param {string} id
       * @param {number} x
       * @param {number} y
       * @param {number} w
       * @param {number} h
       * @returns {vsdui_textinput}
       */function b(a,d,e,f,g){var h;_classCallCheck(this,b),h=_possibleConstructorReturn(this,_getPrototypeOf(b).call(this,a,d,e,f,g,"input"));h.setstyle({padding:"3px","background-color":"white",color:"#900000","text-align":"left",border:"1px solid silver"});var i=h.that,c=_assertThisInitialized(h);return c.formatter=function(a){return a},i.setAttribute("placeholder","?"),i.addEventListener("focus",function(){c.aktvalue=i.value;},!1),i.addEventListener("blur",function(){c.altvalue=c.aktvalue,i.value=c.formatter(i.value),c.aktvalue=i.value,c.aktvalue!==c.altvalue&&(console.log(c.aktvalue,c.altvalue),vsevent.send("changevalue_"+a,{aktwert:c.aktvalue,altwert:c.altvalue,id:a}));},!1),h}/**
       *
       * setzen des feldinhaltes
       * @param {string} v
       * @returns {undefined}
       */return _inherits(b,a),_createClass(b,[{key:"setvalue",value:function setvalue(a){var b=this.that,c=this;b.value=a,c.altvalue=c.aktvalue,b.value=c.formatter(b.value),c.aktvalue=b.value,c.aktvalue!==c.altvalue&&vsevent.send("changevalue_"+b._id,{aktwert:c.aktvalue,altwert:c.altvalue,id:b._id});}/**
       *
       * lesen des feldinhaltes
       * @returns {String}
       */},{key:"getvalue",value:function getvalue(){return this.that.value}/**
       * funktion mit einem parameter übergeben, die für die
       * Formatierung des feldinhaltes sorgt
       * @param {function} f
       * @returns {undefined}
       */},{key:"setformatter",value:function setformatter(a){this.formatter=a;}}]),b}(vsdui_base);

  var vsdui_textoutput=/*#__PURE__*/function(a){/**
     * vsdui_textoutput
     * das gleiche wie textinput, aber nicht änderbar
     *
     * @param {string} id
     * @param {number} x
     * @param {number} y
     * @param {number} w
     * @param {number} h
     * @returns {vsdui_textoutput}
     */function b(a,c,d,e,f){var g;return _classCallCheck(this,b),g=_possibleConstructorReturn(this,_getPrototypeOf(b).call(this,a,c,d,e,f)),g.getcomponent().setAttribute("disabled","true"),g}return _inherits(b,a),b}(vsdui_textinput);

  /*
   * (c) Volker Schulte volkerschulte@gmail.com
   * Änderungswünsche willkommen.
   *
   */ /* ====================================================================== */ /**
   * hallo vsdate
   * @module vsdate
   *
   */var vsdate=/*#__PURE__*/function(){function a(){_classCallCheck(this,a);}return _createClass(a,null,[{key:"formatTime",/**
     *
     *  formatieren eines datetimeobjektes
     * @param {time} t
     * @returns {object} mit attributen gesamt,stunde,minute,sekunde
     */value:function formatTime(a){try{var b="00"+a.getHours();b=b.substr(b.length-2),b=b.replace(/NaN/,"00");var c="00"+a.getMinutes();c=c.substr(c.length-2),c=c.replace(/NaN/,"00");var d="00"+a.getSeconds();return d=d.substr(d.length-2),d=d.replace(/NaN/,"00"),{gesamt:b+":"+c+":"+d,stunde:b,minute:c,sekunde:d}}catch(a){}}/**
     *
     *  vsdate
     *  parsen eines Strings zu einem datetime-objektes
     * @param {string} strwert
     * @returns {datetime}
     */},{key:"parseTime",value:function parseTime(a){var b=a.includes(":"),c="00",d=new Date;if(b){// es sind doppelpunkte da
  var e=a.split(":"),f=c+e[0],g=c+e[1];return d.setHours(f,g),d}return 4===a.length?(d.setHours(a.substr(0,2),a.substr(2)),d):new Date}/**
     *
     *  vsdate
     *  parsen eines Strings zu einem date-objekt
     * @param {string} strwert
     * @returns {date}
     */},{key:"parseDate",value:function parseDate(a){try{var b=a.replace(/\./g,""),c=!1;if(c=a.includes("."),c){if(b.length!==a.length-2)/*
             * falsches format, weil keine zwei punkte!
             */return new Date;if(10<a.length)// wenn schon  punkte, dann MÜSSEN es weniger als 11 stellen sein
  return new Date;var d=a.split("."),e=new Date,f=parseFloat(d[1]);return --f,e.setMonth(f,d[0]),e.setYear(d[2]),e}if(8!==a.length)// wenn schon keine punkte, dann MÜSSEN es 8 stellen sein
  return new Date;var g=new Date;return nr=parseFloat(a.substr(2,2)),--nr,g.setMonth(nr,a.substr(0,2)),g.setYear(a.substr(4)),g}catch(a){return new Date}}/**
     *
     *  vsdate
     *  formatieren eines Strings zu einem datumstring
     * @param {string} strwert
     * @returns {String}
     */},{key:"formatDate",value:function formatDate(a){/*
       * 8 stellen ohne punkte oder mit zwei punkten
       * 10 stellen mit punkte komplettes datum
       *
       */try{var b=a.replace(/\./g,""),c=!1;if(c=a.includes("."),c){if(b.length!==a.length-2)/*
             * falsches format, weil keine zwei punkte!
             */return "";if(10<a.length)// wenn schon  punkte, dann MÜSSEN es weniger als 11 stellen sein
  return "";var d=a.split("."),e=new Date,f=parseFloat(d[1]);--f,e.setMonth(f,d[0]),e.setYear(d[2]);var g=("0000"+e.getDate()).substr(("0000"+e.getDate()).length-2),h=("0000"+(e.getMonth()+1)).substr(("0000"+(e.getMonth()+1)).length-2),i=("0000"+e.getFullYear()).substr(("0000"+e.getFullYear()).length-4);return g+"."+h+"."+i}if(8!==a.length)// wenn schon keine punkte, dann MÜSSEN es 8 stellen sein
  return "";var j=new Date,k=parseFloat(a.substr(2,2));--k,j.setMonth(k,a.substr(0,2)),j.setYear(a.substr(4));var l=("0000"+j.getDate()).substr(("0000"+j.getDate()).length-2),m=("0000"+(j.getMonth()+1)).substr(("0000"+(j.getMonth()+1)).length-2),n=("0000"+j.getFullYear()).substr(("0000"+j.getFullYear()).length-4);return l+"."+m+"."+n}catch(a){}}/**
     *
     *  vsdate
     *  heutiges datum als string zurückgeben
     * @returns {datetime}
     */},{key:"heute",value:function heute(){var a=new Date,b=("0000"+a.getDate()).substr(("0000"+a.getDate()).length-2),c=("0000"+(a.getMonth()+1)).substr(("0000"+(a.getMonth()+1)).length-2),d=("0000"+a.getFullYear()).substr(("0000"+a.getFullYear()).length-4);return b+"."+c+"."+d}/**
     *
     *  datum zu deutschen datumstring
     * @param {date} dt
     * @returns {String}
     */},{key:"dateToDEString",value:function dateToDEString(a){var b=("0000"+a.getDate()).substr(("0000"+a.getDate()).length-2),c=("0000"+(a.getMonth()+1)).substr(("0000"+(a.getMonth()+1)).length-2),d=("0000"+a.getFullYear()).substr(("0000"+a.getFullYear()).length-4);return b+"."+c+"."+d}/**
     *
     *  vsdate
     *  anzahl der tage im angegebenen monat zurückgeben
     * @param {number} jahr
     * @param {number} monat
     * @returns {Number}
     */},{key:"getAnzTageImMonat",value:function getAnzTageImMonat(a,b){var c=new Date(a,b,0);return c.getDate()}/**
     *
     *  vsdate
     *  wochentag als string zurückgeben
     * @param {date} ddatum
     * @returns {String}
     */},{key:"getTagAsString",value:function getTagAsString(a){var b=["Sonntag","Montag","Dienstag","Mittwoch","Donnerstag","Freitag","Samstag"];return a?b[a.getDay()]:b}/**
     *
     * @param {string} strdate  "dd.mm.yyyy" nach yyyymmdd
     * @returns {string}
     */},{key:"ddpmmpyyyy2yyyymmdd",value:function ddpmmpyyyy2yyyymmdd(a){return a.substr(6)+a.substr(3,2)+a.substr(0,2)}/**
     *
     * @param {string} strdate als "yyyymmdd"
     * @returns {string}
     */},{key:"yyyymmdd2ddpmmpyyyy",value:function yyyymmdd2ddpmmpyyyy(a){return a.substr(6)+"."+a.substr(4,2)+"."+a.substr(0,4)}}]),a}();

  var vsdui_label=/*#__PURE__*/function(a){/**
     * vsdui_label
     * label komponente
     *
     * @param {type} id
     * @param {type} text
     * @param {type} x
     * @param {type} y
     * @param {type} w
     * @param {type} h
     * @returns {vsdui_label}
     */function b(a,c,d,e,f,g){var h;_classCallCheck(this,b),h=_possibleConstructorReturn(this,_getPrototypeOf(b).call(this,a,d,e,f,g,"button"));return h.setstyle({padding:"3px","background-color":"white",color:"#900000","text-align":"left"}),h.addcontent(c),h.that.addEventListener("focus",function(){h.that.blur();},!1),h}return _inherits(b,a),b}(vsdui_base);

  var vsdui_panel=/*#__PURE__*/function(a){/**
     * vsdui_panel
     *
     * @param {string} id
     * @param {string} ueberschrift
     * @param {number} x
     * @param {number} y
     * @param {number} w
     * @param {number} h
     */function b(a,c,d,e,f,g){var h;_classCallCheck(this,b),h=_possibleConstructorReturn(this,_getPrototypeOf(b).call(this,a,d,e,f,g,"div"));return h.setstyle({"font-weight":"bold","background-color":"white",color:"#900000",border:"1px solid silver",overflow:"auto"}),h._ueber=new vsdui_label(a+"-bue",c,0,0,f-2,26),h._ueber.setstyle({"background-color":"#0e7a0c",color:"white","text-align":"center"}),h._cont=new vsdui_base(a+"-con",0,28,f-2,g-30),h._cont.setstyle({"background-color":"white",color:"#900000",border:"0px solid",overflow:"auto"}),h.addcontent(h._ueber.getcomponent()),h.addcontent(h._cont.getcomponent()),h}/**
     * gibt die content-komponente zurück
     * @returns {vsdui_panel._cont}
     */return _inherits(b,a),_createClass(b,[{key:"getcontent",value:function getcontent(){return this._cont}}]),b}(vsdui_base);

  /*
   * (c) Volker Schulte volkerschulte@gmail.com
   * Änderungswünsche willkommen.
   *
   */ /**
   *
   *
   * @module vsdom
   *
   *
   */ /** @class vsdom */var vsdom=/*#__PURE__*/function(){function a(){_classCallCheck(this,a);}return _createClass(a,null,[{key:"$",/**
       *
       *  holen eines elementes mittels der id
       * @param {String} id
       * @returns {Element}
       */value:function $(a){return document.getElementById(a)}/**
       *
       *
       *  holen von elementen
       * @param {String} selector
       * @param {Element} startelement
       * @returns {array}
       */},{key:"$$",value:function $$(a){var b=1<arguments.length&&void 0!==arguments[1]?arguments[1]:document;return [].slice.call(b.querySelectorAll(a))}/**
       *
       *  vsdom
       *  erstellt ein Element ggf. mit attributen
       * @param {string} elem  Bezeichnung des Tags
       * @param {object} att  name:value objekt ggf style unterobjekt zum setzen von attributen
       * @param {string|element} text (optional) innerHTML|appendchild wird gesetzt
       * @return {Element} neues DomElement
       *
       *
       */},{key:"makeelem",value:function makeelem(a,b,c){var d=document.createElement(a);if(b)for(var e in b)if("style"==e){var f=b[e];for(var g in f)try{d.style[g]=f[g];}catch(a){}}else d.setAttribute(e,b[e]);return c&&(c instanceof Element?d.appendChild(c):d.innerHTML=c),d}/**
       *
       *  vsdom
       *  setzen von attributen
       * @param {Element} elem
       * @param {object} attr
       * @returns {void}
       */},{key:"setattr",value:function setattr(a,b){for(var c in b)a.setAttribute(c,b[c]);}/**
       *
       *  vsdom
       *  lesen eines attributes
       * @param {Element} elem
       * @param {String} attr
       * @returns {String}
       */},{key:"getattr",value:function getattr(a,b){return a.getAttribute(b)}/**
       *
       *  vsdom
       *  entfernen von attributen
       * @param {element} elem
       * @param {array} attr
       * @returns {void}
       */},{key:"delattr",value:function delattr(a,b){b.forEach(function(b){return a.removeAttribute(b)});}/**
       *
       *
       *  vsdom
       *  löscht ein Element
       * @param {element} elem : element, von dem ein Kindelement gelöscht werden soll
       * @param {element} child : das zu löschende Element als Node!
       *
       */},{key:"delchild",value:function delchild(a,b){a.removeChild(b);}/**
       *
       *  vsdom
       *  löscht alle unterelemente
       * @param {Element} element
       * @returns {void}
       */},{key:"delallchilds",value:function delallchilds(a){try{for(;a.firstChild;)a.removeChild(a.firstChild);}catch(a){}}/**
       *
       *
       *  vsdom
       *  append child
       * @param {element} elem : element, bei dem ein Kindelement angefügt werden soll
       * @param {element} child : das anzufügende Element als Node!
       *
       *
       */},{key:"appchild",value:function appchild(a,b){a.appendChild(b);}/**
       *
       *
       *  vsdom
       *  einfügen Element
       * @param {element} elem : element, bei dem ein Kinelement angefügt werden soll
       * @param {element} child : das anzufügende Element als Node!
       * @param {element} beforechild : vor diesem Element soll eingefügt werden!
       *
       */},{key:"inschildbefore",value:function inschildbefore(a,b,c){a.insertBefore(b,c);}/**
       *
       * [5~
       *  vsdom
       *  einfügen Element
       * @param {element} elem : element, bei dem ein Kinelement angefügt werden soll
       * @param {element} child : das anzufügende Element als Node!
       * @param {element} afterchild : nach diesem Element soll eingefügt werden!
       *
       */},{key:"inschildafter",value:function inschildafter(a,b,c){a.insertBefore(b,c.nextSibling);}}]),a}();

  var vsdui_datepicker=/*#__PURE__*/function(a){/**
     * vsdui_datepicker
     * @description datumsauswahlbox
     * <pre>
     * sendet: 'datepicker' mit ID und VALUE
     * </pre>
     *
     * @param {string} id
     * @param {string} ueberschrift
     * @param {number} x
     * @param {number} y
     * @param {number} w
     * @param {number} h
     */function b(a,d,e,f,g,i){var h;for(var k in _classCallCheck(this,b),h=_possibleConstructorReturn(this,_getPrototypeOf(b).call(this,a,d,e,f,g,i)),h._cbreite=h.getcontent().getposition().w-10,h._choehe=h.getcontent().getposition().h-10,h._bdrittel=(h._cbreite-10)/3,h._hfuenftel=(h._choehe-9)/5,h._anfang=5,h._comps={bttp:new vsdui_button(a+"-bttm","-",h._anfang,h._anfang+1*h._hfuenftel+3,h._bdrittel/2,h._hfuenftel),bmmp:new vsdui_button(a+"-bmmm","-",h._anfang+h._bdrittel+5,h._anfang+1*h._hfuenftel+3,h._bdrittel/2,h._hfuenftel),bjjp:new vsdui_button(a+"-bjjm","-",h._anfang+h._bdrittel+h._bdrittel+10,h._anfang+1*h._hfuenftel+3,h._bdrittel/2,h._hfuenftel),dttt:new vsdui_textoutput(a+"-dttt",h._anfang,h._anfang,h._bdrittel,h._hfuenftel).setstyle({"text-align":"center"}),dtmm:new vsdui_textoutput(a+"-dtmm",h._anfang+h._bdrittel+5,h._anfang,h._bdrittel,h._hfuenftel).setstyle({"text-align":"center"}),dtjj:new vsdui_textoutput(a+"-dtjj",h._anfang+h._bdrittel+h._bdrittel+10,h._anfang,h._bdrittel,h._hfuenftel).setstyle({"text-align":"center"}),bttm:new vsdui_button(a+"-bttp","+",h._anfang+h._bdrittel/2,h._anfang+1*h._hfuenftel+3,h._bdrittel/2,h._hfuenftel),bmmm:new vsdui_button(a+"-bmmp","+",h._anfang+h._bdrittel+5+h._bdrittel/2,h._anfang+1*h._hfuenftel+3,h._bdrittel/2,h._hfuenftel),bjjm:new vsdui_button(a+"-bjjp","+",h._anfang+h._bdrittel+h._bdrittel+10+h._bdrittel/2,h._anfang+1*h._hfuenftel+3,h._bdrittel/2,h._hfuenftel),tagstring:new vsdui_label(a+"-tag","tag als string",h._anfang,h._anfang+2*h._hfuenftel+9,h._cbreite,h._hfuenftel),bja:new vsdui_button(a+"-bja","Ja",h._anfang,h._anfang+3*h._hfuenftel+9,h._bdrittel,h._hfuenftel),bnein:new vsdui_button(a+"-bnein","Nein",h._anfang+h._bdrittel+5,h._anfang+3*h._hfuenftel+9,h._bdrittel,h._hfuenftel)},h._comps)h.getcontent().addcontent(h._comps[k].getcomponent());h._aktdate=new Date;var j=_assertThisInitialized(h),c={bttp:function bttp(){j._aktdate.setDate(j._aktdate.getDate()+1),j.redraw();},bttm:function bttm(){j._aktdate.setDate(j._aktdate.getDate()-1),j.redraw();},bmmp:function bmmp(){j._aktdate.setMonth(j._aktdate.getMonth()+1),j.redraw();},bmmm:function bmmm(){j._aktdate.setMonth(j._aktdate.getMonth()-1),j.redraw();},bjjp:function bjjp(){j._aktdate.setFullYear(j._aktdate.getFullYear()+1),j.redraw();},bjjm:function bjjm(){j._aktdate.setFullYear(j._aktdate.getFullYear()-1),j.redraw();},bja:function bja(){var b=j._comps;vsevent.send("datepicker_"+a,{id:a,value:vsdate.formatDate(b.dttt.getvalue()+"."+b.dtmm.getvalue()+"."+b.dtjj.getvalue())}),vsdom.delchild(vsdom.$(a).parentNode,vsdom.$(a));},bnein:function bnein(){vsdom.delchild(vsdom.$(a).parentNode,vsdom.$(a));}};for(var l in c)vsevent.handle("click_"+a+"-"+l,c[l]);return h.redraw(),h}/**
     *
     * nur interne verwendung
     * @returns {undefined}
     */return _inherits(b,a),_createClass(b,[{key:"redraw",value:function redraw(){var a=this._comps;a.dttt.setvalue(this._aktdate.getDate()),a.dtmm.setvalue(this._aktdate.getMonth()+1),a.dtjj.setvalue(this._aktdate.getFullYear()),a.tagstring.addcontent(vsdate.getTagAsString(this._aktdate));}/**
     *
     * setzen des datums
     * @param {date} d
     * @returns {undefined}
     */},{key:"setdate",value:function setdate(a){this._aktdate=a,this.redraw();}}]),b}(vsdui_panel);

  var vsdui_dateinput=/*#__PURE__*/function(a){/**
       *
       * dateinput-feld
       *
       * @param {string} id
       * @param {number} x
       * @param {number} y
       * @param {number} w
       * @param {number} h
       * @returns {vsdui_dateinput}
       */function b(a,c,d,e,f){var g;return _classCallCheck(this,b),g=_possibleConstructorReturn(this,_getPrototypeOf(b).call(this,a,c,d,e,f)),g.setformatter(vsdate.formatDate),g.setstyle({"text-align":"center"}),g}return _inherits(b,a),_createClass(b,[{key:"getdate",value:function getdate(){return vsdate.formatDate(this.getvalue())}},{key:"setdatepicker",value:function setdatepicker(){var a=this,b=a.getcomponent().id;a.setvalue(vsdate.heute());var c=a.getposition();return this.getcomponent().addEventListener("focus",function(){a.dp=new vsdui_datepicker(b+"-dp","datum",c.x,c.y+c.h,c.w,200),a.dp.appendtodom(document.body),a.dp.setdate(vsdate.parseDate(a.getvalue()));}),vsevent.handle("datepicker_"+b+"-dp",function(b){try{a.setvalue(b.value);}catch(a){console.log(a);}}),this}}]),b}(vsdui_textinput);

  var vsdui_dateoutput=/*#__PURE__*/function(a){/**
     * vsdui_dateoutput
     * datumsausgabe
     *
     * @param {string} id
     * @param {number} x
     * @param {number} y
     * @param {number} w
     * @param {number} h
     * @returns {vsdui_dateoutput}
     */function b(a,c,d,e,f){var g;return _classCallCheck(this,b),g=_possibleConstructorReturn(this,_getPrototypeOf(b).call(this,a,c,d,e,f)),g.getcomponent().setAttribute("readonly","true"),g}return _inherits(b,a),b}(vsdui_dateinput);

  /*
   * (c) Volker Schulte volkerschulte@gmail.com
   * Änderungswünsche willkommen.
   *
   */ /**
   *
   * @module vscss
   *
   *
   */var vscss=/*#__PURE__*/function(){function a(){_classCallCheck(this,a);}return _createClass(a,null,[{key:"setstyle",/**
       *
       *
       * @param {element} e domobjekt
       * @param {object} obj object mit den styleangaben
       */value:function setstyle(a,b){for(var c in b)try{a.style[c]=b[c];}catch(a){}}/**
       *
       *
       *
       * @param {element} e domobjekt
       * @param {object} obj object mit den styleangaben
       */},{key:"delstyle",value:function delstyle(a,b){for(var c in b)try{a.style[c]="";}catch(a){}}/**
       *
       *
       *
       * @param {Element} e
       * @param {String} clazz
       */},{key:"addclass",value:function addclass(a,b){a.classList.contains(b)||a.classList.add(b);}/**
       *
       *
       *
       * @param {Element} e
       * @param {String} clazz
       */},{key:"removeclass",value:function removeclass(a,b){a.classList.remove(b);}/**
       *
       *
       *
       * @param {Element} e
       * @param {String} clazz
       */},{key:"toggleclass",value:function toggleclass(a,b){a.classList.toggle(b);}/**
       *
       *
       *
       * @param {Element} e
       * @param {String} clazz
       */},{key:"setclass",value:function setclass(a,b){a.className=b;}}]),a}();

  var vsdui_grid=/*#__PURE__*/function(a){/**
       * grid komponente
       *
       * @param {string} id
       * @param {number} x
       * @param {number} y
       * @param {number} w
       * @param {number} h
       */function b(a,c,d,e,f){var g;_classCallCheck(this,b),g=_possibleConstructorReturn(this,_getPrototypeOf(b).call(this,a,c,d,e,f,"div"));g.setstyle({border:"0px solid #e0e0e0",overflow:"auto"}),g._id=a,g._zeilenhoehe=30;// let c = this.that;
  var h=new vsdui_base(a+"-con",1,1,e-2,f-2);return g._cont=h,g._printheader=!0,h.setstyle({"background-color":"white",color:"#900000",border:"0px solid",overflow:"auto"}),g.addcontent(h.getcomponent()),g}/**
       *
       * gibt die content-komponente zurück
       * @returns {vsdui_grid._cont}
       */return _inherits(b,a),_createClass(b,[{key:"getcontent",value:function getcontent(){return this._cont}/**
       *
       * @description
       * <pre>
       * [
       * {
       *  title : "meindatenfeld",
       *  ertign : "center",
       *  width : 100,
       *  bind : function(datenzeile){
       *      return datenzeile.datenfeld;
       *  }
       * }
       * ]
       * </pre>
       * @param {arrobjects} b
       * @returns {vsdui_grid}
       */},{key:"setcolstyleandbindings",value:function setcolstyleandbindings(a){return this._colbindings=a,this}/**
       *
       * @param {arrobjects} d
       * @description daten-objekte
       * <pre>
       * [
       * {
       * nachname : "schulte",
       * vorname : "volker"
       * }
       * ]
       * </pre>
       * @returns {vsdui_grid}
       */},{key:"setcoldata",value:function setcoldata(a){return this._coldata=a,this}/**
       *
       * rückgabe der grid-daten
       * @returns {arrayofobjects}
       */},{key:"getcoldata",value:function getcoldata(){return this._coldata}/**
       *
       * festlegen der zeilenhöhe der daten
       * @param {number} zh
       * @returns {vsdui_grid}
       */},{key:"setzeilenhoehe",value:function setzeilenhoehe(a){return this._zeilenhoehe=a,this}/**
       *
       * sollen die spaltenüberschriften mit ausgegeben werden
       * @param {boolean} ph
       * @returns {vsdui_grid}
       */},{key:"setprintheader",value:function setprintheader(a){return this._printheader=a,this}/**
       * highlighten der aktuellen zeile?
       * @param {boolean} hr 
       */},{key:"sethighlightrow",value:function sethighlightrow(a){return this._rowhighlight=a,this}/**
       *
       * erstellen des grid-dom-objektes
       * @returns {undefined}
       */},{key:"render",value:function render(){var a=this;vsdom.delallchilds(this.getcontent().getcomponent());// neue Daten schreiben
  var b=26,d=b;//this.getposition().w
  if(this._printheader){this.header=new vsdui_base(this._id+"-header",0,0,329,b),this.header.setstyle({"background-color":"silver","font-weight":"bold",color:"#900000",border:"0px solid #e0e0e0"});for(var e=this._colbindings.length,f=0,g=0;g<e;g++){var h=this._colbindings[g],c=new vsdui_base(this._id+"-ch-"+g,f,1,h.width,b-4);c.setstyle({"background-color":"#e0e0e0","font-weight":"bold",color:"#900000","border-right":"1px solid silver",overflow:"hidden","text-align":h.align||"center"}),c.addcontent(h.title),f+=h.width,this.header.getcomponent().appendChild(c.getcomponent());}this.header.setposition({w:f+1}),this.getcontent().getcomponent().appendChild(this.header.getcomponent());}else d=0;// jetzt die Datensätze
  var j=this._coldata.length,k=this._colbindings.length,l=0,m=this._coldata,n=this._id;b=this._zeilenhoehe;for(var o=function(c){var e="#f0f0f0";0==c%2&&(e="white");var f=new vsdui_base(n+"-zeile-"+c,0,d,329,b);f.setstyle({"background-color":e}),a.getcontent().getcomponent().appendChild(f.getcomponent()),!0==a._rowhighlight&&(["mouseover","focus"].forEach(function(a){vsdom.$(n+"-zeile-"+c).addEventListener(a,function(){vscss.setstyle(vsdom.$(n+"-zeile-"+c),{"background-color":"lightblue"});});}),["mouseout","blur"].forEach(function(a){vsdom.$(n+"-zeile-"+c).addEventListener(a,function(){vscss.setstyle(vsdom.$(n+"-zeile-"+c),{"background-color":vsdom.$(n+"-zeile-"+c)._style["background-color"]});});})),l=0;for(var i=0;i<k;i++){var g=a._colbindings[i],h=new vsdui_base(a._id+"-cz-"+c+"-cs-"+i,l,0,g.width,b);h.setstyle({"background-color":"transparent",color:"#900000","border-right":"0px solid",overflow:"hidden","text-align":g.align||"center"});try{h.addcontent(g.bind.call(m[c],m[c],c));}catch(a){}l+=g.width,f.getcomponent().appendChild(h.getcomponent());}f.setposition({w:l+1}),d+=b;},p=0;p<j;p++)o(p);}}]),b}(vsdui_base);

  var vsajax=/*#__PURE__*/function(){function a(){_classCallCheck(this,a);}return _createClass(a,null,[{key:"get",/**
       *
       * synchrones holen einer ressource mit get
       * @param {String} url
       * @returns {String} responsetext
       */value:function get(a){var b=new XMLHttpRequest;return b.open("GET",a,!1),b.setRequestHeader("Cache-Control","no-cache"),b.send(null),b.responseText}/**
       *
       *  asynchrones holen einer ressource mit get
       * @param {String} url
       */},{key:"asyncget",value:function asyncget(a){(void 0).onreadystatechange=function(){if(4===b.readyState&&200===b.status)try{vsevent.send("asyncget",a,"nichts",b.responseText);}catch(a){alert(a);}};var b=new XMLHttpRequest;b.open("GET",a,!0),b.setRequestHeader("Cache-Control","no-cache"),b.send(null);}/**
       *
       *  synchrones holen einer ressource mit post
       * @param {String} url
       * @param {String} daten (optional)
       * @returns {String} responsetext
       */},{key:"post",value:function post(a,b){var c=new XMLHttpRequest;return c.open("POST",a,!1),c.setRequestHeader("Method","POST "+a+" HTTP/1.1"),c.setRequestHeader("Content-Type","text/plain;charset=utf-8"),c.setRequestHeader("Cache-Control","no-cache"),c.send(b||null),c.responseText}/**
       *
       *  asynchrones holen einer ressource mit post
       *  es erfolgt ein publish von url,daten,responsetext, also drei parameter beim subscribe
       * @param {String} url
       * @param {String} daten (optional)

       */},{key:"asyncpost",value:function asyncpost(a,b){var c=new XMLHttpRequest;return c.onreadystatechange=function(){if(4===c.readyState&&200===c.status)try{vsevent.send("asyncpost",a,b,c.responseText);}catch(a){alert(a);}},c.open("POST",a,!0),c.setRequestHeader("Method","POST "+a+" HTTP/1.1"),c.setRequestHeader("Content-Type","text/plain;charset=utf-8"),c.setRequestHeader("Cache-Control","no-cache"),c.send(b||null),c.responseText}/**
       *
       *  synchrones holen einer ressource mit post im json-format
       * @param {String} url
       * @param {String} daten (optional)
       * @returns {String} responsetext
       */},{key:"postJSON",value:function postJSON(a,b){var c=new XMLHttpRequest;return c.open("POST",a,!1),c.setRequestHeader("Method","POST "+a+" HTTP/1.1"),c.setRequestHeader("Content-Type","application/json;charset=utf-8"),c.setRequestHeader("Cache-Control","no-cache"),c.send(b||null),c.responseText}/**
       *
       *  asynchrones holen einer ressource mit post im json-format.
       * es erfolgt ein publish von url,daten,responsetext, also drei parameter beim subscribe
       * @param {String} url
       * @param {String} daten (optional)
       */},{key:"asyncpostJSON",value:function asyncpostJSON(a,b){var c=new XMLHttpRequest;c.onreadystatechange=function(){if(4===c.readyState&&200===c.status)try{vsevent.send("asyncpostjson",a,b,c.responseText);}catch(a){alert(a);}},c.open("POST",a,!0),c.setRequestHeader("Method","POST "+a+" HTTP/1.1"),c.setRequestHeader("Content-Type","application/json;charset=utf-8"),c.setRequestHeader("Cache-Control","no-cache"),c.send(b||null);}}]),a}();

  var notizen=/*#__PURE__*/function(){function a(){_classCallCheck(this,a);}return _createClass(a,null,[{key:"getNotizen",value:function getNotizen(a){var b={aktion:"ns",apikey:localStorage.getItem("vskey"),request:{was:a}};return vsajax.postJSON("https://www.vs-hm.de/apps/bashws.vsh",JSON.stringify(b))}},{key:"putNotizen",value:function putNotizen(a,b){var c=vsdate.ddpmmpyyyy2yyyymmdd(a),d={aktion:"nn",apikey:localStorage.getItem("vskey"),request:{wann:c,was:b}};return vsajax.postJSON("https://www.vs-hm.de/apps/bashws.vsh",JSON.stringify(d))}},{key:"updateNotizen",value:function updateNotizen(a,b,c){var d={aktion:"nu",apikey:localStorage.getItem("vskey"),request:{id:a,wann:b,was:c}};return vsajax.postJSON("https://www.vs-hm.de/apps/bashws.vsh",JSON.stringify(d))}},{key:"removeNotizen",value:function removeNotizen(a){var b={aktion:"nd",apikey:localStorage.getItem("vskey"),request:{id:a}};return vsajax.postJSON("https://www.vs-hm.de/apps/bashws.vsh",JSON.stringify(b))}}]),a}();

  var events=/*#__PURE__*/function(){function a(){_classCallCheck(this,a);}return _createClass(a,null,[{key:"makeEvents",value:function makeEvents(){vsevent.handle("click",function(a){if(a.id.startsWith("del_")){var d=notizen.removeNotizen(a.id.substr(4));if(d.includes("error"))return void alert(d);vs.comp.twas.setvalue(""),vsevent.send("click_bsuchen",{});}if(a.id.startsWith("upd_")){var b=a.id.substr(4),c=notizen.updateNotizen(b,vsdom.$("to_"+b).value,vsdom.$("ti_"+b).value);if(c.includes("error"))return void alert(c);vsevent.send("click_bsuchen",{});}}),vsevent.handle("click_bsuchen",function(){var a="";a=""==vs.comp.twas.getvalue()?vsdate.ddpmmpyyyy2yyyymmdd(vs.comp.twann.getdate()):vs.comp.twas.getvalue();var b=[],c=JSON.parse(notizen.getNotizen(a+"*")).data;c.forEach(function(a){var c=a.daten.split("::");b.push({id:a.rowid,wann:c[0],was:c[1]});}),vs.comp.tgrid.setcoldata(b),vs.comp.tgrid.render();}),vsevent.handle("click_baza",function(){var a=vsdate.formatTime(new Date).gesamt,b=notizen.putNotizen(vs.comp.twann.getdate(),"aza "+a);return b.includes("error")?void alert(b):void(vs.comp.twas.setvalue(""),vsevent.send("click_bsuchen",{}))}),vsevent.handle("click_baze",function(){var a=vsdate.formatTime(new Date).gesamt,b=notizen.putNotizen(vs.comp.twann.getdate(),"aze "+a);return b.includes("error")?void alert(b):void(vs.comp.twas.setvalue(""),vsevent.send("click_bsuchen",{}))}),vsevent.handle("click_bneu",function(){var a=notizen.putNotizen(vs.comp.twann.getdate(),vs.comp.twas.getvalue());return a.includes("error")?void alert(a):void(vs.comp.twas.setvalue(""),vsevent.send("click_bsuchen",{}))});}}]),a}();

  window.vs={},window.vs.data={},window.vs.comp={};function vskey(){var a=localStorage.getItem("vskey")||"";(-1<location.href.indexOf("vskey")||null===a||16!==a.length)&&(a=prompt("Apikey eingeben",a)),localStorage.setItem("vskey",a);}function main(){vs.comp.breite=vsdui_window.getwidth(),vs.comp.hoehe=vsdui_window.getheight(),vs.comp.twas=new vsdui_textinput("twas",10,60,vs.comp.breite-20,30),vs.comp.twas.getcomponent().setAttribute("placeholder","was suchen oder eintragen?"),vs.comp.twann=new vsdui_dateoutput("twann",10,100,vs.comp.breite-20,30).setdatepicker(),490<vs.comp.breite&&(vs.comp.twann=new vsdui_dateoutput("twann",10,100,400,30).setdatepicker()),vs.comp.menu=new vsdui_grid("menu",0,0,vs.comp.breite,50),vs.comp.bsuchen=new vsdui_button("bsuchen","Suchen",10,1,80,40),vs.comp.bneu=new vsdui_button("bneu","Neu",10,1,80,40),vs.comp.baza=new vsdui_button("baza","aza",10,1,80,40),vs.comp.baze=new vsdui_button("baze","aze",10,1,80,40),vs.data.menucolstyle=[{width:100,bind:function bind(){return vs.comp.bsuchen.getcomponent()}},{width:100,bind:function bind(){return vs.comp.bneu.getcomponent()}},{width:100,bind:function bind(){return vs.comp.baza.getcomponent()}},{width:100,bind:function bind(){return vs.comp.baze.getcomponent()}}],vs.data.gridcolstyle=[{title:"id",width:102,bind:function bind(a){var d=new vsdui_base("div",1,1,101,48);d.setstyle({"background-color":"transparent"});var c=new vsdui_button("del_"+a.id,"del",1,1,46,46),b=new vsdui_button("upd_"+a.id,"upd",52,1,46,46);return d.addcontent(c.getcomponent()),d.addcontent(b.getcomponent()),d.getcomponent()}},{title:"wann",align:"center",width:100,bind:function bind(a){var b=new vsdui_textoutput("to_"+a.id,1,2,98,46);return b.setvalue(a.wann),b.setstyle({"text-align":"center","background-color":"transparent",border:"0px solid"}),b.getcomponent()}},{title:"was",align:"left",width:1800,bind:function bind(a){var b=new vsdui_textinput("ti_"+a.id,1,2,1800,46);return b.setvalue(a.was),b.setstyle({"background-color":"transparent",border:"0px solid"}),b.getcomponent()}}],vs.comp.tgrid=new vsdui_grid("tgrid",10,140,vs.comp.breite-20,vs.comp.hoehe-150),vs.comp.tgrid.setcolstyleandbindings(vs.data.gridcolstyle).setzeilenhoehe(50).setprintheader(!0).sethighlightrow(!0),document.body.innerHTML="",vs.comp.menu.setprintheader(!1),vs.comp.menu.setzeilenhoehe(44),vs.comp.menu.setcolstyleandbindings(vs.data.menucolstyle),vs.comp.menu.appendtodom(document.body),vs.comp.menu.setcoldata([{}]),vs.comp.menu.render(),vs.comp.twas.appendtodom(document.body),vs.comp.twann.appendtodom(document.body),vs.comp.tgrid.appendtodom(document.body);}vskey(),main(),events.makeEvents();

}());
