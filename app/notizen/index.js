/*eslint-env browser */
import vsdui_button from '../lib/vsdui_button.js';
import vsdui_base from '../lib/vsdui_base.js';
import vswindow from '../lib/vsdui_window.js';
import vsdui_textinput from '../lib/vsdui_textinput.js';
import vsdui_textoutput from '../lib/vsdui_textoutput.js';
import vsdui_dateoutput from '../lib/vsdui_dateoutput.js';
import vsdui_grid from '../lib/vsdui_grid.js';
import vsdui_label from '../lib/vsdui_label.js';
import events from './events.js';

window.vs = {};
window.vs.data = {};
window.vs.comp = {};

function vskey() {

	let k = localStorage.getItem("vskey") || "";
	if (location.href.indexOf("vskey") > -1 || k === null || k.length !== 16) {
		k = prompt("Apikey eingeben", k);
	}
	localStorage.setItem("vskey", k);
}

function main() {
	vs.comp.breite = vswindow.getwidth();
	vs.comp.hoehe = vswindow.getheight();
	vs.comp.twas = new vsdui_textinput("twas", 10, 60, vs.comp.breite - 20, 30);
	vs.comp.twas.getcomponent().setAttribute("placeholder", "was suchen oder eintragen?");
	vs.comp.twann = new vsdui_dateoutput("twann", 10, 100, vs.comp.breite - 20, 30).setdatepicker();
	if (vs.comp.breite > 490) {
		vs.comp.twann = new vsdui_dateoutput("twann", 10, 100, 400, 30).setdatepicker();
	}
	vs.comp.menu = new vsdui_grid("menu", 0, 0, vs.comp.breite, 50);
	vs.comp.bsuchen = new vsdui_button("bsuchen", "Suchen", 10, 1, 80, 40);
	vs.comp.bneu = new vsdui_button("bneu", "Neu", 10, 1, 80, 40);
	vs.comp.baza = new vsdui_button("baza", "aza", 10, 1, 80, 40);
	vs.comp.baze = new vsdui_button("baze", "aze", 10, 1, 80, 40);
	vs.data.menucolstyle = [{
		width: 100,
		bind: function() {
			return vs.comp.bsuchen.getcomponent();
		}
	}, {
		width: 100,
		bind: function() {
			return vs.comp.bneu.getcomponent();
		}
	}, {
		width: 100,
		bind: function() {
			return vs.comp.baza.getcomponent();
		}
	}, {
		width: 100,
		bind: function() {
			return vs.comp.baze.getcomponent();
		}
	}];

	vs.data.gridcolstyle = [{
			title: "id",
			width: 102,
			bind: function(datenzeile) {
				let c = new vsdui_base("div", 1, 1, 101, 48);
                                c.setstyle({
                                    "background-color":"transparent"
                                })
				let b = new vsdui_button("del_" + datenzeile.id, "del", 1, 1, 46, 46);
				let u = new vsdui_button("upd_" + datenzeile.id, "upd", 52, 1, 46, 46);

				c.addcontent(b.getcomponent());
				c.addcontent(u.getcomponent());
				return c.getcomponent();
			}
		},

		{
			title: "wann",
			align: "center",
			width: 100,
			bind: function(datenzeile) {
				let c = new vsdui_textoutput("to_" + datenzeile.id, 1, 2, 98, 46);
				c.setvalue(datenzeile.wann);

                                c.setstyle({
                                    "text-align":"center",
                                    "background-color" : "transparent",
                                    "border":"0px solid"
                                });
                                return c.getcomponent();
			}
		}, {
			title: "was",
			align: "left",
			width: 1800,
			bind: function(datenzeile) {
                            
                            				
				let c = new vsdui_textinput("ti_" + datenzeile.id, 1, 2, 1800, 46);
				c.setvalue(datenzeile.was);

                         
                                c.setstyle({
                                   "background-color" : "transparent",
                                   "border":"0px solid"
                                });
                                return c.getcomponent();

			}
		}
	];

	vs.comp.tgrid = new vsdui_grid("tgrid", 10, 140, vs.comp.breite - 20, vs.comp.hoehe - 150);
	vs.comp.tgrid.setcolstyleandbindings(vs.data.gridcolstyle).setzeilenhoehe(50).setprintheader(true).sethighlightrow(true);

	document.body.innerHTML = "";
	vs.comp.menu.setprintheader(false);
	vs.comp.menu.setzeilenhoehe(44);
	vs.comp.menu.setcolstyleandbindings(vs.data.menucolstyle);
	vs.comp.menu.appendtodom(document.body);
	vs.comp.menu.setcoldata([{}]);
	vs.comp.menu.render();
	vs.comp.twas.appendtodom(document.body);
	vs.comp.twann.appendtodom(document.body);
	vs.comp.tgrid.appendtodom(document.body);

}

vskey();
main();
events.makeEvents();