/*eslint-env es_modules */
/*eslint-disable no-undef */
import vsajax from '../lib/vsajax.js';
import vsdate from '../lib/vsdate.js';


export default class notizen {

    static getNotizen(suchbegriff) {

        let daten = {
            aktion: "ns",
            apikey: localStorage.getItem("vskey"),
            request: {
                was: suchbegriff
            }
        };

        return vsajax.postJSON("https://www.vs-hm.de/apps/bashws.vsh", JSON.stringify(daten));

    }
    static putNotizen(wann, was) {
        let fwann = vsdate.ddpmmpyyyy2yyyymmdd(wann);
        let daten = {
            aktion: "nn",
            apikey: localStorage.getItem("vskey"),
            request: {
                wann: fwann,
                was: was
            }
        };

        return vsajax.postJSON("https://www.vs-hm.de/apps/bashws.vsh", JSON.stringify(daten));
    }
    static updateNotizen(id, wann, was) {
        let daten = {
            aktion: "nu",
            apikey: localStorage.getItem("vskey"),
            request: {
                id: id,
                wann: wann,
                was: was
            }
        };

        return vsajax.postJSON("https://www.vs-hm.de/apps/bashws.vsh", JSON.stringify(daten));

    }
    static removeNotizen(id) {
        let daten = {
            aktion: "nd",
            apikey: localStorage.getItem("vskey"),
            request: {
                id: id
            }
        };

        return vsajax.postJSON("https://www.vs-hm.de/apps/bashws.vsh", JSON.stringify(daten));

    }

}
